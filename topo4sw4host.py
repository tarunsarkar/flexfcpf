#!/usr/bin/env python

from mininet.cli import CLI
from mininet.net import Mininet
from mininet.node import RemoteController
from mininet.link import TCLink
from mininet.node import CPULimitedHost
from mininet.topo import Topo
import functools
from mininet.node import OVSSwitch

if '__main__' == __name__:

    topo = Topo(link=TCLink, host=CPULimitedHost)
    s1 = topo.addSwitch('s1')
    s2 = topo.addSwitch('s2')
    s3 = topo.addSwitch('s3')
    s4 = topo.addSwitch('s4')

    h1 = topo.addHost('h1')
    h2 = topo.addHost('h2')
    h3 = topo.addHost('h3')
    h4 = topo.addHost('h4')

    linkopts = dict()
    topo.addLink(s1, h1, **linkopts)
    topo.addLink(s2, h2, **linkopts)
    topo.addLink(s3, h3, **linkopts)
    topo.addLink(s4, h4, **linkopts)

    topo.addLink(s1, s2, **linkopts)
    topo.addLink(s2, s3, **linkopts)
    topo.addLink(s3, s4, **linkopts)
    switch = functools.partial(OVSSwitch, protocols='OpenFlow13')
    net = Mininet(topo=topo, switch=switch, controller=RemoteController, host=CPULimitedHost, link=TCLink)

    net.start()
    net.startTerms()
    CLI(net)
    net.stop()
