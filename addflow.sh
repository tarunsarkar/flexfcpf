#!/bin/bash

curl -X PUT -d '{"addflow" : 1, "inport" : 1, "action" : 2, "dltype" : "ip", "ipsrc" : "10.0.0.1", "ipdest" : "10.0.0.4"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000001
curl -X PUT -d '{"addflow" : 1, "action" : 2, "dltype" : "arp", "ipsrc" : "10.0.0.1", "ipdest" : "10.0.0.4"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000001
curl -X PUT -d '{"addflow" : 1, "inport" : 3, "action" : 1, "dltype" : "ip", "ipsrc" : "10.0.0.4", "ipdest" : "10.0.0.1"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000001
curl -X PUT -d '{"addflow" : 1, "action" : 1, "dltype" : "arp", "ipsrc" : "10.0.0.4", "ipdest" : "10.0.0.1"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000001
curl -X PUT -d '{"addflow" : 1, "inport" : 2, "action" : 3, "dltype" : "ip", "ipsrc" : "10.0.0.1", "ipdest" : "10.0.0.4"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000002
curl -X PUT -d '{"addflow" : 1, "action" : 3, "dltype" : "arp", "ipsrc" : "10.0.0.1", "ipdest" : "10.0.0.4"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000002
curl -X PUT -d '{"addflow" : 1, "inport" : 2, "action" : 1, "dltype" : "ip", "ipsrc" : "10.0.0.1", "ipdest" : "10.0.0.4"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000004
curl -X PUT -d '{"addflow" : 1, "action" : 1, "dltype" : "arp", "ipsrc" : "10.0.0.1", "ipdest" : "10.0.0.4"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000004
curl -X PUT -d '{"addflow" : 1, "inport" : 1, "action" : 3, "dltype" : "ip", "ipsrc" : "10.0.0.4", "ipdest" : "10.0.0.1"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000004
curl -X PUT -d '{"addflow" : 1, "action" : 3, "dltype" : "arp", "ipsrc" : "10.0.0.4", "ipdest" : "10.0.0.1"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000004
curl -X PUT -d '{"addflow" : 1, "inport" : 3, "action" : 2, "dltype" : "ip", "ipsrc" : "10.0.0.4", "ipdest" : "10.0.0.1"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000003
curl -X PUT -d '{"addflow" : 1, "action" : 2, "dltype" : "arp", "ipsrc" : "10.0.0.4", "ipdest" : "10.0.0.1"}' http://127.0.0.1:8080/myswitch/flowtable/0000000000000003

ovs-ofctl -O OpenFlow13 dump-flows s1
ovs-ofctl -O OpenFlow13 dump-flows s2
ovs-ofctl -O OpenFlow13 dump-flows s3
ovs-ofctl -O OpenFlow13 dump-flows s4

