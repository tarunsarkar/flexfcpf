from __future__ import division
import sys, math, random, time, pdb
from math import exp, sqrt, pow
from cp_flex_fcfs import *
import datetime
import thread
from mininet.cli import CLI
from crowd_network import ftfile

# initialize
try:
    emulator = str(sys.argv[1])
except:
    emulator = "Mininet"

print "Info: Emulator " + emulator + " is used."

try:
    sim_duration = int(sys.argv[2])
except:
    sim_duration = 600

print "Info: The test will run for " + str(sim_duration) + " seconds"

try:
    filename = str(sys.argv[3])
except:
    filename = 'Test_9_mesh.dat'


print "Info: The test will use topology specified in " + filename

cpf = CPFlex(filename, emulator=emulator)
# cpf = CPFlex(filename, emulator=emulator, evalscen="CoMP")
cpf.flexOperation = True
cpf.clearFlows()
cpf.cpgreedy()
no_nodes = len(cpf.cn.V)

cpf.setupEmulationNetwork()
cpf.TestbedNetwork.startTerms()

time.sleep(10)
cpf.populateNetworkLinks()
cpf.modifyRoutingTable()
for n in cpf.cn.G.node:
    node = cpf.cn.G.node[n]
    pathToCLC = node['pathtoCLC']
    print n
    print pathToCLC


while True:
    var = raw_input("Please enter anything to add DFG and break to end emulation : ")
    print "you entered", var
    if var == "break":
        break
    elif var == "genquick":
        cpf.cn.evalscen = "generic"
        cpf.cn.loadflowtypes(ftfile[cpf.cn.evalscen])
        cpf.addFlow(stime=0, dur=0.05)
        fid = cpf.cn.F[-1]
        n = cpf.cn.Wb[cpf.cn.F[-1]][0]
        print "Simple DFG originating from " + str(n) \
              + " satisfied by LCA " + str(cpf.cn.fdata[cpf.cn.F[-1]]['CLC'])
        delay = cpf.cn.fdata[fid]['p_flow'] / cpf.cn.G.node[cpf.cn.fdata[fid]['CLC']]['ProcFlow'][fid] * 1000
        bucket_id = int(round(delay * 10))
        id = bucket_id + n * 1000
        bucket_id = bucket_id + n * 65536
        hexFid = "0x%0.8x" % bucket_id
        print "BucketId = " + str(hexFid)
    elif var == "generic":
        cpf.cn.evalscen = "generic"
        cpf.cn.loadflowtypes(ftfile[cpf.cn.evalscen])
        cpf.addFlow(stime=0, dur=1000)
    elif var == "CoMP":
        cpf.cn.evalscen = "CoMP"
        cpf.cn.loadflowtypes(ftfile[cpf.cn.evalscen])
        cpf.addFlow(stime=0, dur=1000)
    cpf.generateTrafficForSingleFlow(cpf.cn.F[-1])
    #cpf.modifyRoutingTable()


if emulator == "Mininet":
    # Mininet
    # cpf.TestbedNetwork.startTerms()
    CLI(cpf.TestbedNetwork)
elif emulator == "MaxiNet":
    # MaxiNet
    cpf.TestbedNetwork.CLI(locals(), globals())
else:
    print("Error: Emulator missing!")
    exit(1)

cpf.TestbedNetwork.stop()

