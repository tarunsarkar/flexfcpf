
\documentclass[10pt,oneside]{article}

\usepackage{times}
\usepackage{url}

\usepackage{geometry}
\geometry{a4paper,body={5.8in,9in}}
%\geometry{a4paper,body={6in,9.8in}}

% Graphics:
\usepackage{graphicx}
% aller Bilder werden im Unterverzeichnis figures gesucht:
\graphicspath{{figures/}}

\usepackage{subfigure}

\usepackage[cmex10]{amsmath}
\interdisplaylinepenalty=2500

\usepackage{clrscode}
\usepackage{algorithmic}
\usepackage{algorithm}

\usepackage{enumerate}

\usepackage{eqparbox}

\usepackage{fixltx2e}

\usepackage{url}

\usepackage{xcolor}
\usepackage{amssymb}
\usepackage{calc}
\usepackage{booktabs}

% no indent
\usepackage{parskip}

\newcommand{\refFig}[1]{Figure \ref{#1}}
\newcommand{\refSec}[1]{Section \ref{#1}}
\newcommand{\refCap}[1]{Chapter \ref{#1}}
\newcommand{\refTab}[1]{Table \ref{#1}}
\newcommand{\refEq}[1]{Equation \ref{#1}}
\newcommand{\refAlg}[1]{Algorithm \ref{#1}}

\newcommand{\ti}[1]{\todo[inline]{#1}}
\newcommand{\td}[1]{\todo{#1}}

% ========== Abkürzungen ==========
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\seq}{\subseteq}
\newcommand{\sneq}{\subsetneq}

\definecolor{algComment}{rgb}{0.4,0.4,0.4}
\renewcommand{\algorithmiccomment}[1]{\textcolor{algComment}{// #1}}

\usepackage[printonlyused]{acronym}

\begin{document}

\begin{acronym}
\acro{CLC}{CROWD Local Controller}
\acro{CRC}{CROWD Regional Controller}
\acro{FCFS}{First Come First Serve}
\acro{FCPF}{Flow processing-aware Controller Placement Framework}
\acro{MIQCP}{Mixed Integer Quadratically Constrained Program}
\end{acronym}

\section*{FCPF optimisation model with first come first serve scheduling}

The \ac{FCPF} optimisation model with \ac{FCFS} scheduling is a \ac{MIQCP}. It takes the parameters listed in \refTab{tab:sol_cp_params} as inputs and uses the variables listed in \refTab{tab:sol_cp_vars} to store the decisions about controller placement and data flow satisfaction.

\begin{table}[htb]
\begin{center}
\caption{MIQCP input parameters}
\begin{tabular}{ll}
\toprule
$V$ & set of nodes (WiFi APs, LTE BSs, routers/switches)\\
$C \seq V$ & set of nodes that can serve as controller (CRC or CLC)\\
$E$ & set of links with $E \subseteq V \times V$\\
$F$ & set of flows passing through at least one node $v \in V$\\
$W$ & matrix with $W_{x,v} = 1$ iff $x \in F$ passes through $v \in V$\\
$p_{\mathrm{own}}(c)$ & processing power at node $c \in C$\\
$b_{\mathrm{cap}}(v,w)$ & maximum data rate for link $(v,w) \in E$\\
$l_{\mathrm{cap}}(v,w)$ & latency of link $(v,w) \in E$\\
$b_{\mathrm{flow}}(x)$ & requested data rate for flow $x \in F$ to its CLC\\
$l_{\mathrm{flow}}(x)$ & required round trip latency for flow $x \in F$ to its CLC\\
$p_{\mathrm{flow}}(x)$ & processing capacity required for flow $x \in F$ at a CLC\\
%$t_{\mathrm{in}}(x)$ & arrival time of data flow $x \in F$\\
%$t_{\mathrm{dur}}(x)$ & duration of data flow $x \in F$\\
$b_{\mathrm{CLC}}$ & required data rate for a node to its CLC\\
$b_{\mathrm{CRC}}$ & required data for a CLC to its CRC\\
$l_{\mathrm{CLC}}$ & required round trip latency for a node to its CLC\\
$l_{\mathrm{CRC}}$ & required round trip latency for a CLC to its CRC\\
$p_{\mathrm{CLC}}$ & processing cap. required at a CLC for controlling a node\\
$p_{\mathrm{CRC}}$ & processing cap. required at a CRC for controlling a CLC\\
\bottomrule
\end{tabular} 
%\vspace{-0.5cm}
\label{tab:sol_cp_params}
\end{center}
\end{table}

\begin{table}[thb]
\begin{center}
\caption{MIQCP variables}
\begin{tabular}{ll}
\toprule
$CLC_{c,v} \in \{0,1\}$ & determines whether $c \in C$ is a CLC for $v \in V$\\
$CRC_{c,d} \in \{0,1\}$ & det. whether $c \in C$ is the CRC for CLC $d \in C$\\
$isCLC_{c} \in \{0,1\}$ & determines whether $c \in C$ is a CLC\\
$isCRC_{c} \in \{0,1\}$ & determines whether $c \in C$ is a CRC\\
$Sat_{c,x} \in \{0,1\}$ & determines whether $c \in C$ satisfies $x \in F$\\
$isSat_{x} \in \{0,1\}$ & determines whether $x \in F$ is satisfied by a CLC \\
$p_{c,d}^{\text{CRC}} \in \R_+$ & processing capacity reserved at $c \in C$ for $d \in C$\\
$p_{c,v}^{\text{CLC}} \in \R_+$ & processing capacity reserved at $c \in C$ for $v \in V$\\
$p_{c,x}^{\text{Flow}} \in \R_+$ & processing capacity reserved at $c \in C$ for $x \in F$\\
$f_{c,u,v,w} \in \{0,1\}$ & determines whether $(v,w) \in E$ is included in the \\& path from CLC $c \in C$ to node $u \in V$\\
$g_{c,d,v,w} \in \{0,1\}$ & determines whether $(v,w) \in E$ is included in the \\&  path from CRC $c \in C$ to CLC $d \in C$\\
\bottomrule
\end{tabular} 
%\vspace{-0.5cm}
\label{tab:sol_cp_vars}
\end{center}
\end{table}

\newpage

The following constraints have to be ensured. Some constraints use a big-M constant denoted as $\mathcal{M}$.

All nodes need to be the end of a routing path (\ref{CLCstart}) that is starting at a \ac{CLC} (\ref{CLCend}). For all intermediate nodes on a routing path, the ingress and egress have to be balanced (\ref{CLCinout}).
%\small
\begin{align}
\sum_{(c,w) \in E} f_{c,d,c,w} &= CLC_{c,d}, \quad\forall c \in C, d \in V, c \neq d \label{CLCstart}\\
\sum_{(u,d) \in E} f_{c,d,u,d} &= CLC_{c,d}, \quad\forall c \in C, d \in V, c \neq d \label{CLCend}\\
\sum_{(u,v) \in E} f_{c,d,u,v} &= \sum_{(v,w) \in E} f_{c,d,v,w}, \quad
\begin{array}{@{}c@{}} \forall c \in C,\, d,v \in V,\, \\ c \neq d \neq v \end{array} \label{CLCinout}
\end{align}
\normalsize

Similar constraints are needed for the routing paths from each \ac{CLC} to its \ac{CRC} (\ref{CRCstart}-\ref{CRCinout}).
%\small
\begin{align}
\sum_{(c,w) \in E} g_{c,d,c,w} &= CRC_{c,d}\cdot isCLC_{d}, \quad\forall c,d \in C, c \neq d \label{CRCstart}\\
\sum_{(v,d) \in E} g_{c,d,v,d} &= CRC_{c,d}\cdot isCLC_{d}, \quad\forall c,d \in C, c \neq d \label{CRCend}\\
\sum_{(u,v) \in E} g_{c,d,u,v} &= \sum_{(v,w) \in E} g_{c,d,v,w}, \quad
\begin{array}{@{}c@{}} \forall c,d \in C,\, v \in V,\, \\ c \neq d \neq v \end{array} \label{CRCinout}
\end{align}
\normalsize

Each node is required to be controlled by at least one \ac{CLC} (\ref{requireCLC}) and each \ac{CLC} must be assigned to exactly one \ac{CRC} (\ref{requireCRC}).
%\small
\begin{align}
\sum_{c \in C} CLC_{c,v} &\geq 1, \quad\forall v \in V \label{requireCLC}\\
\sum_{c \in C} CRC_{c,d} &= isCLC_{d}, \quad\forall d \in C \label{requireCRC}
\end{align}
\normalsize

Further, the \ac{CLC} (\ref{CLCaux1}) and \ac{CRC} (\ref{CRCaux1}) decision variables need to be set.
%\small
\begin{align}
\mathcal{M} \cdot isCLC_{c} \geq \sum_{v \in V} CLC_{c,v}, \quad\forall c \in C \label{CLCaux1}\\
%isCLC_{c} \leq \sum_{v \in V} CLC_{c,v}, \quad\forall c \in C \label{CLCaux2}\\
\mathcal{M} \cdot isCRC_{c} \geq \sum_{d \in C} CRC_{c,d}, \quad\forall c \in C \label{CRCaux1}
%isCRC_{c} \leq \sum_{d \in C} CRC_{c,d}, \quad\forall c \in C \label{CRCaux2}
\end{align}
\normalsize

A \ac{CLC} can only satisfy a flow if it controls all nodes a flow is connected to. If this is not the case, constraint (\ref{onlySatifconnected}) forces the corresponding decision variable to be zero.
%\small
\begin{align}
\mathcal{M} \cdot (Sat_{c,x} - 1) \leq \sum_{v \in V} (CLC_{c,v} - 1) \cdot W_{x,v},\quad \begin{array}{@{}c@{}} \forall c \in C,\\ x \in F \end{array} \label{onlySatifconnected}
\end{align}
\normalsize

A data flow needs to be satisfied by at most one \ac{CLC} (\ref{oneSatperflow}) and the corresponding decision variable needs to be set (\ref{Sataux}).
%\small
\begin{align}
&\sum_{c \in C} Sat_{c,x} \leq 1, \quad\forall x \in F \label{oneSatperflow}\\
&isSat_{x} = \sum_{c \in C} Sat_{c,x} , \quad\forall x \in F \label{Sataux}
\end{align}
\normalsize

Moreover, the data rate limits of each link (\ref{dataratelinks}) must not be exceeded.
%\small
\begin{align}
&\sum_{c \in C, d \in V} f_{c,d,v,w} \cdot \bigg(b_{\mathrm{CLC}} + \sum_{x \in F} W_{x,d} \cdot Sat_{c,x} \cdot b_{\mathrm{flow}}(x)\bigg) \nonumber\\ 
&\quad\quad\quad + \sum_{c,d \in C} g_{c,d,v,w} \cdot b_{\mathrm{CRC}} \leq  b_{\mathrm{cap}}(v,w), \quad\forall (v,w) \in E \label{dataratelinks}
\end{align}
\normalsize

Now, we need to make sure that enough processing capacity is provided to fulfil the latency constraints for each \ac{CRC} (\ref{latencyCRCs}), \ac{CLC} (\ref{latencyCLCs}) and data flow (\ref{latencyFlows}).
%\small
\begin{align}
&\sum_{(v,w) \in E} f_{c,d,v,w} \cdot (l_{\mathrm{cap}}(v,w) + l_{\mathrm{cap}}(w,v)) + \frac{p_{\mathrm{flow}}(x)}{p_{c,x}^{\text{Flow}}} \label{latencyFlows}\\
&\quad\quad\quad\leq l_{\mathrm{flow}}(x) + \mathcal{M} \cdot (1 - Sat_{c,x}), \quad\forall c \in C, d \in V, x \in F, W_{x,d} = 1 \nonumber\\
&\sum_{(v,w) \in E} f_{c,d,v,w} \cdot (l_{\mathrm{cap}}(v,w) + l_{\mathrm{cap}}(w,v)) + \frac{p_{\mathrm{CLC}}}{p_{c,d}^{\text{CLC}}} \label{latencyCLCs}\\
&\quad\quad\quad\leq l_{\mathrm{CLC}} + \mathcal{M} \cdot (1 - CLC_{c,d}), \quad\forall c \in C, d \in V \nonumber\\
&\sum_{(v,w) \in E} g_{c,d,v,w} \cdot (l_{\mathrm{cap}}(v,w) + l_{\mathrm{cap}}(w,v)) + \frac{p_{\mathrm{CRC}}}{p_{c,d}^{\text{CRC}}} \label{latencyCRCs}\\
&\quad\quad\quad\leq l_{\mathrm{CRC}} + \mathcal{M} \cdot (1 - CRC_{c,d}), \quad\forall c,d \in C \nonumber
\end{align}
\normalsize

But processing capacity should only be provided to controlled nodes and satisfied data flows (\ref{procCRC} - \ref{procflow}).
%\small
\begin{align}
&p_{c,x}^{\text{Flow}} \leq \mathcal{M} \cdot Sat_{c,x}, \quad\forall c \in C, x \in F \label{procflow}\\
&p_{c,v}^{\text{CLC}} \leq \mathcal{M} \cdot CLC_{c,v}, \quad\forall c \in C, v \in V \label{procCLC} \\
&p_{c,d}^{\text{CRC}} \leq \mathcal{M} \cdot CRC_{c,d}, \quad\forall c,d \in C \label{procCRC}
\end{align}
\normalsize

Also, we cannot assign more processing power than available at a controller (\ref{maxproc}).
%\small
\begin{align}
\sum_{d \in C} p_{c,d}^{\text{CRC}} + \sum_{v \in V} p_{c,v}^{\text{CLC}} + \sum_{x \in F} p_{c,x}^{\text{Flow}} \leq p_{\mathrm{node}}(c), \quad\forall c \in C\label{maxproc}
\end{align}
\normalsize

Last but not least, we need to cope with possible loop and corner cases (\ref{CLCloop1}-\ref{corner2}).
%\small
\begin{align}
\sum_{(v,c) \in E} f_{c,d,v,c} &= 0, \quad\forall c \in C, d \in V, c \neq d \label{CLCloop1}\\
\sum_{(d,w) \in E} f_{c,d,d,w} &= 0, \quad\forall c \in C, d \in V, c \neq d \label{CLCloop2}\\
\sum_{(v,c) \in E} g_{c,d,v,c} &= 0, \quad\forall c,d \in C, c \neq d \label{CRCloop1}\\
\sum_{(d,w) \in E} g_{c,d,d,w} &= 0, \quad\forall c,d \in C, c \neq d \label{CRCloop2}\\
CLC_{c,c} &= isCLC_{c}, \quad\forall c \in C  \label{corner1}\\
CRC_{c,c} &= isCRC_{c} \cdot isCLC_{c}, \quad\forall c \in C \label{corner2}
\end{align}
\normalsize

The objective function (\ref{of}) defines a lexicographic order in which maximising flow satisfaction weighs higher than minimising the number of controllers.
%\small 
\begin{align}
\text{\begin{normalsize}minimise:\end{normalsize}}\,\, &\sum_{c \in C} \big( isCRC_{c} + isCLC_{c} \big) - 3|C| \cdot \sum_{x \in F} isSat_{x} \label{of}
\end{align}
\normalsize

\end{document}

