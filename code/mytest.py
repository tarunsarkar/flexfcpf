from __future__ import division
import sys, math, random, time, pdb
from math import exp, sqrt, pow
from cp_flex_fcfs import *
from scapy.all import *

filename = 'Test_36_mesh.dat'

cpf = CPFlex(filename)
cpf.setupEmulationNetwork()

cpf.cpgreedy()
print "Start Adding routing entry:" + str(time.time())
cpf.modifyRoutingTable()
print "Start the iperf client:" + str(time.time())
cpf.generateTrafficFlow()

for switch in cpf.TestbedNetwork.switches:
        print cpf.TestbedNetwork.get(switch.name).dpctl("-O OpenFlow13 dump-flows")
        # command = "ovs-ofctl -O OpenFlow13 add-flow %s" % switch.name + " ip,in_port=1" + ",nw_src=" + "10.0.0.1" + ",nw_dst=" + "10.0.0.2" + ",action=output:2"
        # print cpf.TestbedNetwork.get_worker(switch).run_cmd(command)
        # print command

cpf.TestbedNetwork.CLI(locals(), globals())
cpf.TestbedNetwork.stop()


# ovs-vsctl --timeout=1 list-br
# ovs-vsctl --if-exists del-br s1
# ovs-vsctl --timeout=1 list-br
# *** Removing all links of the pattern foo-ethX
# ip link show | egrep -o '([-_.[:alnum:]]+-eth[[:digit:]]+)'
# ( ip link del s1-eth1 ) 2> /dev/null
# ip link show
# *** Killing stale mininet node processes
# pkill -9 -f mininet:
# *** Shutting down stale tunnels
# pkill -9 -f Tunnel=Ethernet
# pkill -9 -f .ssh/mn
# rm -f ~/.ssh/mn/*

