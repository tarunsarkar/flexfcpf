from __future__ import division
import sys
import math
#from pprint import pprint
import pylab as P
import numpy as np
from matplotlib.transforms import Bbox
from matplotlib.font_manager import FontProperties
from collections import defaultdict

labels = {}
labels["nodes"] = "Number of nodes" 
labels["#flows"] = "Number of flows" 
labels["#Satisfied"] = "Flows satisfied (%)" 
labels["#CRCs"] = "Number of CRCs used" 
labels["#CLCs"] = "Number of CLCs used" 
labels["#CLCDiff"] = "Number of new CLCs" 
labels["#nodeDiff"] = "New node assignments" 
labels["#flowDiff"] = "New flow assignments" 
labels["CRCpathlength"] = "Average CRC path length" 
labels["CLCpathlength"] = "Average CLC path length" 
labels["CLCload"] = "Average CLC load" 
labels["controlRatio"] = "CLC control ratio" 
labels["runtime"] = "runtime (s)" 
labels["netmodtime"] = "netmodtime (s)"

plot_path = "plots_sim/plots3"
results_path = "Simulation_Results"

results = {}
no_nodes = 36
Topology = ["mesh","ring"]
FlowType = ["gen","comp"]
Platform = ["sim","emu"]
obj = ["#Satisfied","#CRCs","#CLCs","#CLCDiff","#nodeDiff","#flowDiff","CRCpathlength","CLCpathlength","CLCload","controlRatio","runtime", "netmodtime"]

for t in Topology:
	results[t] = {}
	for f in FlowType:
		results[t][f] = {}
		for p in Platform:
			results[t][f][p] = {}
			resultsfile = results_path + "/" + p + "_results_pfo_fcfs_Test_" + str(no_nodes) + "_" + t + "_" + f + ".dat"
			results[t][f][p]["counter"] = 0
			results[t][f][p]["flows"] = 0
			results[t][f][p]["info"] = {}
			for o in obj:
				results[t][f][p]["info"][o] = 0

			fin = open(resultsfile, "r")
			tmp = fin.readline()

			while True:
				tmp = fin.readline().split(" ")
				try:
					results[t][f][p]["counter"] += 1
					results[t][f][p]["flows"] += int(tmp[1])
					for i in range(0,6):
						results[t][f][p]["info"][obj[i]] += int(tmp[i+2])
					for i in range(6,len(obj)):
						results[t][f][p]["info"][obj[i]] += float(tmp[i+2])
				except:
					break

for t in Topology:
	for f in FlowType:
		for p in Platform:
			results[t][f][p]["flows"] /= results[t][f][p]["counter"]
			for o in obj:
				results[t][f][p]["info"][o] /= results[t][f][p]["counter"]

markers = ['^','*','o','v']
font = FontProperties(size=28)
font_smaller = FontProperties(size=18)
			
for o in obj:
	print "Creating graph for " + str(o)
	plotlabels = []
	plotlines = []
	
	y_label = labels[o]
	F = P.figure()
	AX1 = F.add_subplot(111)
	AX1.set_ylabel(y_label, fontproperties=font)
	ind = np.arange(len(Topology)*len(FlowType))
	width = 0.35
	AX1.set_xticks(ind+width)
	AX1.set_xticklabels([str(t)+str(f) for t in Topology for f in FlowType])
	
	ysim = []
	yemu = []
	
	for t in Topology:
		for f in FlowType:
			ysim.append(results[t][f]["sim"]["info"][o])
			yemu.append(results[t][f]["emu"]["info"][o])

	pl = AX1.bar(ind, ysim, width, color='blue')
	plotlines.append(pl[0])
	plotlabels.append("Reassignment")
	pls = AX1.bar(ind+width, yemu, width, color='red')
	plotlines.append(pls[0])
	plotlabels.append("Scratch comparison")
	
	AX1.set_ylim(ymin=0)
	#AX1.set_yscale('log')
	for tick in AX1.xaxis.get_major_ticks():
		tick.label1.set_fontsize(24)
	for tick in AX1.yaxis.get_major_ticks():
		tick.label1.set_fontsize(24)
	P.savefig(plot_path + '/plot_' + str(o).replace("#","") + '.pdf', bbox_inches='tight')
	F = P.figure()
	F.legend(plotlines, plotlabels, loc='upper left', shadow=False, fancybox=True, prop=font_smaller)
	bb = Bbox.from_bounds(0, 0, 6.4, 4)
	P.savefig(plot_path + '/plot_legend_1_col.pdf', bbox_inches=bb)
	F = P.figure()
	F.legend(plotlines, plotlabels, loc='upper left', shadow=False, fancybox=True, prop=font_smaller, ncol=2)
	bb = Bbox.from_bounds(0, 0, 7, 2)
	P.savefig(plot_path + '/plot_legend_2_col.pdf', bbox_inches=bb)
			
		
