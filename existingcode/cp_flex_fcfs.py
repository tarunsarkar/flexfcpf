from __future__ import division
import networkx as nx
import sys, math, random, time, copy
from crowd_network import *
import pdb
#pdb.set_trace()

class CPFlex:

	def __init__(self, filename=None, flowOption="MostDemanding", scenario=None, modify_controllers=False, contrProb=None, cn=None, inputversion="flex"):
		if filename is not None:
			if scenario is None:
				read_weights_from_file = True
			else:
				read_weights_from_file = False

			self.cn = CrowdNetwork()
			valid_network = self.cn.generate_from_file(filename, read_weights_from_file, scenario, modify_controllers, contrProb, inputversion)
			if valid_network:
				self.state = "NOT SOLVED"
			else:
				self.state = "INVALID NETWORK"
				print "Error: Invalid network!"
				exit(1)
		else:
			if cn is None:
				print "Error: Nothing to create CPFlex network from!"
				exit(1)
			else:
				self.cn = cn
				self.state = "NOT SOLVED"
			
		self.getFlowOption = flowOption
		self.iterations = 0
		self.Controlled = []
		self.CRCs = []
		self.CLCs = []
		self.Satisfied = []
		self.uncontrolledCLCs = []
		self.CLCloadlimit = 0.5
		self.remCLCswithlowload = False
		self.flexOperation = False
		if len(self.cn.C) > 1:
			self.VCRatio = len(self.cn.V)/(len(self.cn.C)-1)
		else: 
			self.VCRatio = len(self.cn.V)
			
			
	def scratchCopy(self):
		cntmp = self.cn.copy()
		cntmp.cleanup()
		
		return CPFlex(filename=None,cn=cntmp)

	def cpgreedy(self):
		if self.state == "INVALID NETWORK":
			print "Error: Invalid network!"
			exit(1)
			
		self.iterations += 1
		self.newCLCcontrols = 0 # for simulation stats
		self.newFlowSats = 0
			
		if self.iterations > 1:	
			if self.remCLCswithlowload == True:
				tmp = list(self.CLCs)
				for c in tmp:
					if self.CLCload(c) < self.CLCloadlimit:
						self.remCLC(c)
			while len(self.uncontrolledCLCs) > 0:
				v = self.uncontrolledCLCs[0]
				Gtmp = self.cn.G.copy()
				Gtmp = self.findCRC(Gtmp,v)
				if Gtmp is not None:
					self.cn.G = Gtmp
				else:
					self.remCLC(v)
				self.uncontrolledCLCs.remove(v)
			self.updateVCRatio()
			self.browseCurrentCLCs()
			
		if len(self.Controlled) < len(self.cn.V):
			self.state = "NOT SOLVED"
			
		self.globalOption = "neighbors"
		while len(self.Controlled) < len(self.cn.V):
			self.findCLC(self.globalOption)
			if len(self.CLCs) == len(self.cn.C):
				break
				
		if len(self.Controlled) == len(self.cn.V):
			self.state = "Solved"
		
		if len(self.CLCs) < len(self.cn.C):
			self.globalOption = "flows"		
			while len(self.Satisfied) < len(self.cn.F):
				tmpsat = len(self.Satisfied)
				self.findCLC(self.globalOption)
				if len(self.CLCs) == len(self.cn.C) or tmpsat == len(self.Satisfied):
					if tmpsat == len(self.Satisfied):
						self.newCLCcontrols -= len(self.cn.G.node[self.CLCs[-1]]['CLCcontrol'])
						self.remCLC(self.CLCs[-1])
					break		
		self.cleanupCLCcontrols(self.cn.V)
		
	def findCLC(self,option):
		#tstart = time.time()
		candidates = self.getCLCcandidates(option)
		#tend = time.time()
		#print "Candidate-Runtime: " + str(tend-tstart)
		for v in candidates:
			Gtmp = self.cn.G.copy()
			Gtmp = self.findCRC(Gtmp,v)
			if Gtmp is not None:
				self.cn.G = self.addNewCLC(Gtmp,v)
				break
			
	def getCLCcandidates(self,option=None):
	
		candidates = list(set(self.cn.C) - set(self.CLCs))
		# avoid CRCs to be used as CLCs as long as possible
		if len(set(candidates) - set(self.CRCs)) > 0:
			candidates = [c for c in candidates if not c in self.CRCs]
		remaining_nodes = set(self.cn.V) - set(self.Controlled)
		remaining_flows = set(self.cn.F) - set(self.Satisfied)
		
		if option == "neighbors":
			ctmp = [(k,len((set([k]) | set(self.cn.G.neighbors(k))) - set(self.Controlled))) for k in candidates]
			ctmp.sort(key=lambda x: x[1], reverse=True)
			bestvalue = ctmp[0][1] 
			if bestvalue > 0:
				candidates = [x[0] for x in ctmp]
			else:
				self.globalOption = "isolated_nodes"
				candidates = self.getCLCcandidates("isolated_nodes")
		elif option == "isolated_nodes":
			paths = []
			for i in remaining_nodes:
				for j in candidates:
					paths.append(nx.shortest_path(self.cn.G, source=j, target=i))
			paths.sort(key=len)
			candidates = []
			for p in paths:
				if not p[0] in candidates:
					candidates.append(p[0])
		elif option == "flows":
			ctmp = [(k,len(set(self.cn.Wf[k]) - set(self.Satisfied))) for k in candidates]
			ctmp.sort(key=lambda x: x[1], reverse=True)
			bestvalue = ctmp[0][1]
			if bestvalue > 0:
				candidates = [x[0] for x in ctmp]
			else:
				self.globalOption = "isolated_flows"
				candidates = self.getCLCcandidates("isolated_flows")
		elif option == "flows_nn": # CAUTION: very slow for many flows in the network! Currently not used.
			ctmp = [(k,len(set(self.cn.Wf[k]) - set(self.Satisfied)) + sum(len(set(self.cn.Wf[j]) - set(self.Satisfied)) for j in self.cn.G.neighbors(k))) for k in candidates]
			ctmp.sort(key=lambda x: x[1], reverse=True)
			bestvalue = ctmp[0][1]
			if bestvalue > 0:
				candidates = [x[0] for x in ctmp]
			else:
				self.globalOption = "isolated_flows"
				candidates = self.getCLCcandidates("isolated_flows")
		elif option == "isolated_flows":
			paths = []
			for f in remaining_flows:
				for i in self.cn.Wb[f]:
					for j in candidates:
						paths.append(nx.shortest_path(self.cn.G, source=j, target=i))
			paths.sort(key=len)
			candidates = []
			for p in paths:
				if not p[0] in candidates:
					candidates.append(p[0])
		elif option == "isolated_flows2": # just a test, currently not used.
			nodes_with_flows = [(k,len(set(self.cn.Wf[k]) - set(self.Satisfied))) for k in self.cn.V]
			nodes_with_flows.sort(key=lambda x: x[1], reverse=True)
			node_with_most_flows = nodes_with_flows[0][0]
			paths = []
			for j in candidates:
				paths.append(nx.shortest_path(self.cn.G, source=j, target=node_with_most_flows))
			paths.sort(key=len)
			candidates = [p[0] for p in paths]
		elif option == "neighbors_and_flows": # currently not used.
			ctmp = [(k,len(set([k]) | set(self.cn.G.neighbors(k)) - set(self.Controlled)) + len(set(self.cn.Wf[k]) - set(self.Satisfied))) for k in candidates]
			ctmp.sort(key=lambda x: x[1], reverse=True)
			candidates = [x[0] for x in ctmp]
			
		return candidates
			
	def addNewCLC(self,G,v):
		paths = []
		pf = set([])
		nc = 0
		nnc = 0
		fs = 0
		
		tmp = self.checkCLC(G,[v])
		if tmp == True:
			nc += 1
			if v not in self.Controlled:
				nnc +=1
			G = self.addCLCcontrol(G,[v])
			pf = self.updatePotentialFlows(pf,G,v,[v])
		else:
			self.cn.C.remove(v)
			return G
			
		for i in self.cn.V:
			if i <> v and (i not in self.Controlled or len(set(self.cn.Wf[i]) - set(self.Satisfied)) > 0):
				paths.append((nx.shortest_path(G, source=v, target=i), i in self.Controlled, len(set(self.cn.Wf[i]) - set(self.Satisfied))))
		if len(self.Controlled) < len(self.cn.V):
			paths.sort(key=lambda x: x[1])
			paths.sort(key=lambda x: len(x[0]))
			notyetsolved = True
		else:
			paths.sort(key=lambda x: len(x[0]))
			paths.sort(key=lambda x: x[2], reverse=True)
			notyetsolved = False
			
		while (len(paths) > 0 or len(pf) > 0) and (len(self.Controlled) < len(self.cn.V) or len(self.Satisfied) < len(self.cn.F)):			
			#print "Controlled: "  + str(len(self.Controlled)) + " / " + str(len(self.cn.V)) + "   Satisfied: " + str(len(self.Satisfied)) + " / " + str(len(self.cn.F))
			#print "Current CLC: " + str(v) + "   NNC: " + str(nnc) + "   VCRatio: " + str(self.VCRatio)
			#time.sleep(0.1)
			if notyetsolved and len(self.Controlled) == len(self.cn.V):
				paths.sort(key=lambda x: len(x[0]))
				paths.sort(key=lambda x: x[2], reverse=True)
				notyetsolved = False
			if len(pf) > 0 and (nnc >= self.VCRatio or len(self.Controlled) == len(self.cn.V)):
				f = self.getFlow(pf, self.getFlowOption)
				tmp = self.checkFlowSat(G,v,f)
				#print "CLC: " + str(v) + " Flow: " + str(f) + " Result: " + str(tmp)
				if tmp == True:
					G = self.addFlowSat(G,v,f)
					fs += 1
				pf.remove(f)
			else:
				p = list(paths[0][0])
				del paths[0]
				tmp = self.checkCLC(G,p)
				#print "CLC: " + str(v) + " Node: " + str(p[-1]) + " Result: " + str(tmp)
				if tmp == True:
					nc += 1
					if p[-1] not in self.Controlled:
						nnc +=1
					G = self.addCLCcontrol(G,p)
					pf = self.updatePotentialFlows(pf,G,v,[p[-1]])
				elif tmp > 2:	
					break
				
		return G
		
	def updateVCRatio(self):
		self.VCRatio = (len(self.cn.V)-len(self.Controlled)) / len(self.cn.C)
		
	def updatePotentialFlows(self,pf,G,v,nn):
		cpf = set([])
		for w in nn:
			cpf = cpf | set(self.cn.Wf[w])
		for f in cpf:
			if self.cn.fdata[f]['isSat'] == False and set(self.cn.Wb[f]) <= set(G.node[v]['CLCcontrol']):
				pf.add(f)
			
		return pf
		
	def getFlow(self, pf, option):
		tmp = list(pf)
		if len(tmp) == 0:
			return None
		else:
			if option == "MostDemanding":
				tmp.sort(key=lambda f: self.cn.fdata[f]['p_flow'], reverse=True)
			elif option == "LeastDemanding":
				tmp.sort(key=lambda f: self.cn.fdata[f]['p_flow'])
			return tmp[0]	
	
	def findCRC(self, G, v):
		# check already active CRCs first
		paths = []
		for i in self.CRCs: 
			paths.append(nx.shortest_path(G, source=i, target=v))
		paths.sort(key=len)
		for p in paths:
			if self.checkCRC(G,p):
				return self.addCRCcontrol(G,p)
		
		# need to add a new CRC, at first try to avoid active CLCs and CLC candidate
		return self.findNewCRC(G,v)
		
	def findNewCRC(self, G, v):	
		paths = []
		for i in list(set(self.cn.C) - (set(self.CRCs) | set(self.CLCs) | set([v]))):
			paths.append(nx.shortest_path(G, source=i, target=v))
		if len(self.CRCs) == 0:	# first CRC should be placed centrally
			paths.sort(key=lambda p: sum(len(nx.shortest_path(G, source=p[0], target=c)) for c in self.cn.C))
		else:	
			paths.sort(key=lambda p: len(p))
		for p in paths:
			if self.checkCRC(G,p):
				return self.addCRCcontrol(G,p)
		# last option: try CLC candidate, then already active CLCs
		if self.checkCRC(G,[v]):
			return self.addCRCcontrol(G,[v])
		paths = []
		for i in self.CLCs:
			paths.append(nx.shortest_path(G, source=i, target=v))
		paths.sort(key=len)
		for p in paths:
			if self.checkCRC(G,p):
				return self.addCRCcontrol(G,p)
						
		return None
		
	def addCRCcontrol(self,G,path):
		v = path[0]
		w = path[-1]
		for i in range(0,len(path)-1):
			G.edge[path[i]][path[i+1]]['b_rem'] -= self.cn.b_CRC
		G.node[v]['p_rem'] -= self.cn.p_CRC/(self.cn.l_CRC - sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)))
		G.node[v]['ProcCRC'][w] = self.cn.p_CRC/(self.cn.l_CRC - sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)))
		G.node[v]['CRCcontrol'].append(w)
		G.node[v]['CRCpaths'][w] = path
		G.node[v]['isCRC'] = True
		G.node[w]['CRC'] = v
		G.node[w]['pathtoCRC'] = path
		if v not in self.CRCs:
			self.CRCs.append(v)
		
		return G
			
	def addCLCcontrol(self,G,path):
		self.newCLCcontrols += 1
		v = path[0]
		w = path[-1]
		for i in range(0,len(path)-1):
			G.edge[path[i]][path[i+1]]['b_rem'] -= self.cn.b_CLC
		G.node[v]['p_rem'] -= self.cn.p_CLC/(self.cn.l_CLC - sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)))
		G.node[v]['ProcCLC'][w] = self.cn.p_CLC/(self.cn.l_CLC - sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)))
		G.node[v]['CLCcontrol'].append(w)
		G.node[v]['CLCpaths'][w] = path
		G.node[v]['isCLC'] = True
		G.node[w]['CLCs'].append(v)
		G.node[w]['pathtoCLC'][v] = path
		if v not in self.CLCs:
			self.CLCs.append(v)
		if w not in self.Controlled:
			self.Controlled.append(w)
		
		return G
		
	def addFlowSat(self,G,v,f):
		self.newFlowSats += 1
		self.cn.fdata[f]['isSat'] = True
		self.cn.fdata[f]['CLC'] = v
		flowpaths = [G.node[v]['CLCpaths'][k] for k in self.cn.Wb[f]]
		for path in flowpaths:
			for i in range(0,len(path)-1):
				G.edge[path[i]][path[i+1]]['b_rem'] -= self.cn.fdata[f]['b_flow']
		maxpathlatency = max([sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)) for path in flowpaths])
		G.node[v]['p_rem'] -= self.cn.fdata[f]['p_flow']/(self.cn.fdata[f]['l_flow'] - maxpathlatency)
		G.node[v]['ProcFlow'][f] = self.cn.fdata[f]['p_flow']/(self.cn.fdata[f]['l_flow'] - maxpathlatency)
		G.node[v]['Satisfies'].append(f)
		self.Satisfied.append(f)
		
		return G
	
	# checks if a certain CRC control can be established	
	def checkCRC(self,G,path):
		v = path[0]
		for i in range(0,len(path)-1):
			if G.edge[path[i]][path[i+1]]['b_rem'] < self.cn.b_CRC:
				return False
		if sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)) + self.cn.p_CRC/G.node[v]['p_rem'] > self.cn.l_CRC:
			return False
		
		return True
	
	# checks if a certain CLC control can be established
	def checkCLC(self,G,path):
		v = path[0]
		for i in range(0,len(path)-1):
			if G.edge[path[i]][path[i+1]]['b_rem'] < self.cn.b_CLC:
				return 2
		if sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)) + self.cn.p_CLC/G.node[v]['p_rem'] > self.cn.l_CLC:
			return 4
		
		return True
		
	# checks if a flow f can be satisfied by a controller v
	def checkFlowSat(self,G,v,f):
		if self.cn.fdata[f]['isSat'] == True:
			return False
		if not set(self.cn.Wb[f]) <= set(G.node[v]['CLCcontrol']):
			return False
		for k in self.cn.Wb[f]:
			path = G.node[v]['CLCpaths'][k]
			for i in range(0,len(path)-1):
				if G.edge[path[i]][path[i+1]]['b_rem'] < self.cn.fdata[f]['b_flow']:
					return False
			if sum(2*G.edge[path[i]][path[i+1]]['l_cap'] for i in range(0,len(path)-1)) + self.cn.fdata[f]['p_flow']/G.node[v]['p_rem'] > self.cn.fdata[f]['l_flow']:
				return False
		
		return True
		
	def remCRC(self,v):
		self.cn.G.node[v]['isCRC'] = False
		tmp = list(self.cn.G.node[v]['CRCcontrol'])
		for w in tmp:
			self.remCRCcontrol(v,w)
		self.CRCs.remove(v)	
		
	def remCRCcontrol(self,v,w):
		path = self.cn.G.node[v]['CRCpaths'][w]
		for i in range(0,len(path)-1):
			self.cn.G.edge[path[i]][path[i+1]]['b_rem'] += self.cn.b_CRC
		self.cn.G.node[v]['p_rem'] += self.cn.G.node[v]['ProcCRC'][w]
		del self.cn.G.node[v]['ProcCRC'][w]
		self.cn.G.node[v]['CRCcontrol'].remove(w)
		del self.cn.G.node[v]['CRCpaths'][w]
		self.cn.G.node[w]['CRC'] = None
		self.cn.G.node[w]['pathtoCRC'] = None
		if self.cn.G.node[w]['isCLC'] == True:
			self.uncontrolledCLCs.append(w)		
			self.state = "NOT SOLVED"
		if len(self.cn.G.node[v]['CRCcontrol']) == 0:
			self.remCRC(v)
		
	def remCLC(self,v):
		tmp = list(self.cn.G.node[v]['Satisfies'])
		for f in tmp:
			self.remFlowSat(f)
		tmp = list(self.cn.G.node[v]['CLCcontrol'])
		for w in tmp:
			self.remCLCcontrol(v,w)	
		self.cn.G.node[v]['isCLC'] = False
		if self.cn.G.node[v]['CRC'] is not None:
			self.remCRCcontrol(self.cn.G.node[v]['CRC'],v)
		self.CLCs.remove(v)
			
	def remCLCcontrol(self,v,w):
		path = self.cn.G.node[v]['CLCpaths'][w]
		for i in range(0,len(path)-1):
			self.cn.G.edge[path[i]][path[i+1]]['b_rem'] += self.cn.b_CLC
		self.cn.G.node[v]['p_rem'] += self.cn.G.node[v]['ProcCLC'][w]
		del self.cn.G.node[v]['ProcCLC'][w]
		self.cn.G.node[v]['CLCcontrol'].remove(w)
		del self.cn.G.node[v]['CLCpaths'][w]
		self.cn.G.node[w]['CLCs'].remove(v)
		del self.cn.G.node[w]['pathtoCLC'][v]
		if len(self.cn.G.node[w]['CLCs']) == 0:
			self.Controlled.remove(w)
			self.state = "NOT SOLVED"
		
	def remFlowSat(self,f):
		v = self.cn.fdata[f]['CLC']
		for k in self.cn.Wb[f]:
			path = self.cn.G.node[v]['CLCpaths'][k]
			for i in range(0,len(path)-1):
				self.cn.G.edge[path[i]][path[i+1]]['b_rem'] += self.cn.fdata[f]['b_flow']
		self.cn.G.node[v]['p_rem'] += self.cn.G.node[v]['ProcFlow'][f]
		del self.cn.G.node[v]['ProcFlow'][f]
		self.cn.G.node[v]['Satisfies'].remove(f)
		self.cn.fdata[f]['isSat'] = False
		self.cn.fdata[f]['CLC'] = None
		self.Satisfied.remove(f)
		
	def addFlow(self,stime=0,dur=None,amount=1):
		for i in range(1,amount+1):
			self.cn.addFlow(stime=stime,dur=dur)
		if self.flexOperation and len(self.CLCs) > 0:
			if amount == 1:
				self.browseCurrentCLCsforSingleFlow(self.cn.F[-1])
			else:
				self.browseCurrentCLCs()
		
	def remFlow(self,f):
		#self.cn.fdata[-f] = copy.deepcopy(self.cn.fdata[f]) # uncomment ONLY for debbuging!
		if self.cn.fdata[f]['isSat'] == True:
			self.remFlowSat(f)
		tmplist = list(self.cn.Wb[f])
		self.cn.remFlow(f)
		if self.flexOperation:
			self.cleanupCLCcontrols(tmplist)
		
	def clearFlows(self):
		tmp = list(self.cn.F)
		for f in tmp:
			self.remFlow(f)
						
	def browseCurrentCLCs(self):
		paths = []
		pf = {}
		nnc = {}
		for v in self.CLCs:
			if self.CLCload(v) < 0.99:
				pf[v] = self.updatePotentialFlows(set([]),self.cn.G,v,self.cn.G.node[v]['CLCcontrol'])
				nnc[v] = 0
				for i in list(set(self.cn.V) - set(self.cn.G.node[v]['CLCcontrol'])):
					if i not in self.Controlled or len(set(self.cn.Wf[i]) - set(self.Satisfied)) > 0:
						paths.append((nx.shortest_path(self.cn.G, source=v, target=i), i in self.Controlled, len(set(self.cn.Wf[i]) - set(self.Satisfied))))
					
		if len(self.Controlled) < len(self.cn.V):
			paths.sort(key=lambda x: x[1])
			paths.sort(key=lambda x: len(x[0]))
			notyetsolved = True
		else:
			paths.sort(key=lambda x: len(x[0]))
			paths.sort(key=lambda x: x[2], reverse=True)
			notyetsolved = False
				
		while (len(paths) > 0 or sum(len(pf[v]) for v in pf) > 0) and (len(self.Controlled) < len(self.cn.V) or len(self.Satisfied) < len(self.cn.F)):			
			if notyetsolved and len(self.Controlled) == len(self.cn.V):
				paths.sort(key=lambda x: len(x[0]))
				paths.sort(key=lambda x: x[2], reverse=True)
				notyetsolved = False

			if sum(len(pf[v]) for v in pf) > 0 and (len(paths) == 0 or len(self.Controlled) == len(self.cn.V)):
				currv = [v for v in pf if len(pf[v]) > 0][0]
			else: 
				currv = paths[0][0][0]
				
			if self.CLCload(currv) > 0.99:
				pf[currv] = set([])	
				paths = [p for p in paths if p[0][0] <> currv]
			elif len(pf[currv]) > 0 and (len(paths) == 0 or nnc[currv] >= self.VCRatio or len(self.Controlled) == len(self.cn.V)):
				f = self.getFlow(pf[currv], self.getFlowOption)
				tmp = self.checkFlowSat(self.cn.G,currv,f)
				if tmp == True:
					self.cn.G = self.addFlowSat(self.cn.G,currv,f)
					for w in pf:
						if f in pf[w]:
							pf[w].remove(f)
				else:
					pf[currv].remove(f)
			else:
				p = list(paths[0][0])
				del paths[0]
				tmp = self.checkCLC(self.cn.G,p)
				if tmp == True:
					if p[-1] not in self.Controlled:
						nnc[currv] +=1
					self.cn.G = self.addCLCcontrol(self.cn.G,p)
					pf[currv] = self.updatePotentialFlows(pf[currv],self.cn.G,currv,[p[-1]])
				elif tmp > 2:	
					paths = [p for p in paths if p[0][0] <> currv]
					pf[currv] = set([])
						
	def CLCload(self,c):
		if not c in self.CLCs:
			return 0

		return 1.0 - self.cn.G.node[c]['p_rem']/self.cn.G.node[c]['p_node']
		
	def CLCoutput(self,c):
		out = "Data for CLC " + str(c) + ":\n"
		out += "Load: " + str(self.CLCload(c))  + "\n"
		out += "p_rem: " + str(self.cn.G.node[c]['p_rem']) + ", Nodes controlled: " + str(len(self.cn.G.node[c]['CLCcontrol'])) + ", Flows satisfied: " + str(len(self.cn.G.node[c]['Satisfies'])) + "\n"
		if len(self.cn.G.node[c]['Satisfies']) > 0:
			out += "Biggest flow satisfied: " + str(max(self.cn.fdata[f]['p_flow'] for f in self.cn.G.node[c]['Satisfies'])) + "\n"
		
		return out
		
	def getAverageCLCload(self):
		return sum(self.CLCload(c) for c in self.CLCs)/len(self.CLCs)
		
	def getCLCwithLeastLoad(self):
		tmp = list(self.CLCs)
		tmp.sort(key=lambda c: self.CLCload(c))
		return tmp[0]
		
	def getAverageCLCpathlength(self):
		return sum(len(self.cn.G.node[c]['CLCpaths'][v]) for c in self.CLCs for v in self.cn.G.node[c]['CLCcontrol']) / sum(len(self.cn.G.node[c]['CLCcontrol']) for c in self.CLCs)
		
	def getAverageCRCpathlength(self):
		return sum(len(self.cn.G.node[c]['CRCpaths'][v]) for c in self.CRCs for v in self.cn.G.node[c]['CRCcontrol']) / sum(len(self.cn.G.node[c]['CRCcontrol']) for c in self.CRCs)
		
	def CLCcontrolRatio(self):
		return sum(len(self.cn.G.node[c]['CLCcontrol']) for c in self.CLCs)/len(self.cn.V)
		
	def cleanupCLCcontrols(self,nodelist):
		counter = 0
		vtmp = list(nodelist)
		random.shuffle(vtmp)
		for v in vtmp:
			ctmp = list(self.cn.G.node[v]['CLCs'])
			random.shuffle(ctmp)
			for c in ctmp:
				if c <> v and len(self.cn.G.node[v]['CLCs']) > 1 and len(set(self.cn.G.node[c]['Satisfies']) & set(self.cn.Wf[v])) == 0:
					self.remCLCcontrol(c,v)
					counter += 1
		#print "CLC controls removed: " + str(counter)
		
	def browseCurrentCLCsforSingleFlow(self,f):
		CLCstmp = list([c for c in self.CLCs if set(self.cn.Wb[f]) <= set(self.cn.G.node[c]['CLCcontrol'])])
		for c in CLCstmp:	
			tmp = self.checkFlowSat(self.cn.G,c,f)
			if tmp == True:
				self.cn.G = self.addFlowSat(self.cn.G,c,f)
				return 1
		
		CLCstmp = list([c for c in self.CLCs if not set(self.cn.Wb[f]) <= set(self.cn.G.node[c]['CLCcontrol'])])
		CLCstmp.sort(key=lambda c: sum(len(nx.shortest_path(self.cn.G, source=c, target=i)) for i in self.cn.Wb[f]))
		CLCstmp.sort(key=lambda c: len(set(self.cn.Wb[f]) - set(self.cn.G.node[c]['CLCcontrol'])))
		for c in CLCstmp:
			paths = (nx.shortest_path(self.cn.G, source=c, target=i) for i in self.cn.Wb[f] if not i in self.cn.G.node[c]['CLCcontrol'])
			for p in paths:
				tmp = self.checkCLC(self.cn.G,p)
				if tmp == True:
					self.cn.G = self.addCLCcontrol(self.cn.G,p)
				else: 
					break
			if tmp == True:
				tmp = self.checkFlowSat(self.cn.G,c,f)
				if tmp == True:
					self.cn.G = self.addFlowSat(self.cn.G,c,f)
				else: 
					for p in paths:
						self.remCLCcontrol(c,p[-1])