#!/bin/bash

# copy iperf outfile from remote
scp maxinet@fgcn-crowd-2:/tmp/iperf_*.log .
scp maxinet@fgcn-crowd-3:/tmp/iperf_*.log .
scp maxinet@fgcn-crowd-4:/tmp/iperf_*.log .

# clean master
ps -eaf|grep MaxiNetFrontend|awk '{print "sudo kill -9 " $2}'|sh
ps -eaf|grep fcpf_ryu_controller|awk '{print "kill -9 " $2}'|sh
sudo mn -c
screen -d -m -S MaxiNetFrontend MaxiNetFrontendServer
screen -d -m -S fcpf_ryu_controller /home/maxinet/ryu/bin/ryu-manager --observe-links /home/crowd/flexfcpf/fcpf_ryu_controller.py

# Clean worker
ps -eaf|grep mininet|awk '{print "sudo kill -9 " $2}'|sh
ps -eaf|grep MaxiNetWorker|awk '{print "sudo kill -9 " $2}'|sh
sudo mn -c
sudo screen -d -m -S MaxiNetWorker MaxiNetWorker


# get link
curl -X GET http://127.0.0.1:8080/fcpfcontroller/topology/links

#run test

sudo python csimpfo_fcfs.py Mininet 1200 Test_9_mesh.dat
python csimpfo_fcfs.py MaxiNet 1200 Test_36_mesh.dat

python crun_flexfcpf.py Mininet Test_9_mesh.dat
python crun_flexfcpf.py MaxiNet Test_36_mesh.dat


# generate report
python traffic_distribution.py

# tc command help
tc qdisc add dev h1-eth0 root handle 1: htb

tc class add dev h1-eth0 parent 1: classid 1:1 htb rate 750Mbit
tc qdisc add dev h1-eth0 parent 1:1 handle 20: netem delay 200ms
tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:1 match ip dst 10.0.0.2/32

tc class add dev h1-eth0 parent 1: classid 1:2 htb rate 750Mbit
tc qdisc add dev h1-eth0 parent 1:2 handle 30: netem delay 300ms
tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:2 match ip dst 10.0.0.3/32

tc class add dev h1-eth0 parent 1: classid 1:3 htb rate 750Mbit
tc qdisc add dev h1-eth0 parent 1:3 handle 40: netem delay 400ms
tc filter add  u32 classid 1:3 match ip dst 10.0.0.4/32

tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:1 match ip src 10.0.0.3/32 match ip dst 10.0.0.2/32 match ip tos 0x10 1e

tc qdisc add dev h1-eth0 root handle 1: htb

tc class add dev h1-eth0 parent 1: classid 1:1 htb rate 750Mbit
tc qdisc add dev h1-eth0 parent 1:1 handle 20: netem delay 200ms
tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:1 match ip dst 10.0.0.2/32

tc class add dev h1-eth0 parent 1: classid 1:2 htb rate 750Mbit
tc qdisc add dev h1-eth0 parent 1:2 handle 30: netem delay 300ms
tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:2 match ip dst 10.0.0.3/32

tc class add dev h1-eth0 parent 1: classid 1:3 htb rate 750Mbit
tc qdisc add dev h1-eth0 parent 1:3 handle 40: netem delay 400ms
tc filter add  u32 classid 1:3 match ip dst 10.0.0.4/32

tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:1 match ip src 10.0.0.3/32 match ip dst 10.0.0.2/32 match ip tos 0x10 1e


tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 flow map key dst addend 10.0.0.2/32
mininet> h1 tc qdisc add dev h1-eth0 parent 1:1 handle 20: netem delay 200ms
mininet> h1 tc filter add dev h1-eth0 parent 1:0 protocol ip prio 1 u32 classid 1:1 match u32 0x0a000002 0xffffffff at 16


#!/bin/bash
for i in {100..900}
do
   sudo rm iperf_client_$i*.log
done


mininet> h0 tc class show dev h0-eth0
class htb 1:50 root leaf 50: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:500 root leaf 500: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:100 root leaf 100: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
mininet> h0 tc qdisc show dev h0-eth0
qdisc htb 1: root refcnt 2 r2q 10 default 0 direct_packets_stat 369913 direct_qlen 1000
qdisc netem 100: parent 1:100 limit 1000 delay 10.0ms
qdisc netem 500: parent 1:500 limit 1000 delay 50.0ms
qdisc netem 50: parent 1:50 limit 1000 delay 5.0ms
mininet> h0 tc filter show dev h0-eth0
filter parent 1: protocol ip pref 1 u32
filter parent 1: protocol ip pref 1 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 1 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 100:1
  match 00000064/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::801 order 2049 key ht 800 bkt 0 flowid 500:1
  match 000001f4/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::802 order 2050 key ht 800 bkt 0 flowid 50:1
  match 00000032/ffffffff at 28
mininet>




mininet> h8 tc qdisc show dev h8-eth0
qdisc htb 1: root refcnt 2 r2q 10 default 0 direct_packets_stat 6 direct_qlen 1000
qdisc netem 500: parent 1:500 limit 1000 delay 500.0ms
qdisc netem 50: parent 1:50 limit 1000 delay 50.0ms

mininet> h2 tc qdisc show dev h2-eth0
qdisc htb 1: root refcnt 2 r2q 10 default 0 direct_packets_stat 171536 direct_qlen 1000
qdisc netem 50: parent 1:50 limit 1000 delay 5.0ms
qdisc netem 100: parent 1:100 limit 1000 delay 10.0ms
qdisc netem 500: parent 1:500 limit 1000 delay 50.0ms

mininet> h8 tc class show dev h8-eth0
class htb 1:50 root leaf 50: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:500 root leaf 500: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b

mininet> h2 tc class show dev h2-eth0
class htb 1:500 root leaf 500: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:100 root leaf 100: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:50 root leaf 50: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b

mininet> h8 tc filter show dev h8-eth0
filter parent 1: protocol ip pref 1 u32
filter parent 1: protocol ip pref 1 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 1 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 1:500
  match 000001f4/ffffffff at 28
filter parent 1: protocol ip pref 2 u32
filter parent 1: protocol ip pref 2 u32 fh 801: ht divisor 1
filter parent 1: protocol ip pref 2 u32 fh 801::800 order 2048 key ht 801 bkt 0 flowid 1:50
  match 00000032/ffffffff at 28

mininet> h2 tc filter show dev h2-eth0
filter parent 1: protocol ip pref 1 u32
filter parent 1: protocol ip pref 1 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 1 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 50:1
  match 00000032/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::801 order 2049 key ht 800 bkt 0 flowid 100:1
  match 00000064/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::802 order 2050 key ht 800 bkt 0 flowid 500:1
  match 000001f4/ffffffff at 28


=======================
MaxiNet> h11 tc qdisc show dev h11-eth0
qdisc htb 1: root refcnt 2 r2q 10 default 0 direct_packets_stat 234131 direct_qlen 1000
qdisc netem 50: parent 1:50 limit 1000 delay 5.0ms
qdisc netem 49: parent 1:49 limit 1000 delay 4.9ms
qdisc netem 99: parent 1:99 limit 1000 delay 9.9ms
MaxiNet> h11 tc class show dev h11-eth0
class htb 1:99 root leaf 99: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:49 root leaf 49: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:50 root leaf 50: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
MaxiNet> h11 tc filter show dev h11-eth0
filter parent 1: protocol ip pref 1 u32
filter parent 1: protocol ip pref 1 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 1 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 50:1
  match 00000032/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::801 order 2049 key ht 800 bkt 0 flowid 49:1
  match 00000031/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::802 order 2050 key ht 800 bkt 0 flowid 99:1
  match 00000063/ffffffff at 28

MaxiNet> h10 tc class show dev h10-eth0
class htb 1:100 root leaf 100: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
class htb 1:500 root leaf 500: prio 0 rate 750000Kbit ceil 750000Kbit burst 1500b cburst 1500b
MaxiNet> h11 tc qdisc show dev h11-eth0
qdisc htb 1: root refcnt 2 r2q 10 default 0 direct_packets_stat 437286 direct_qlen 1000
qdisc netem 50: parent 1:50 limit 1000 delay 5.0ms
qdisc netem 49: parent 1:49 limit 1000 delay 4.9ms
qdisc netem 99: parent 1:99 limit 1000 delay 9.9ms
MaxiNet> h10 tc qdisc show dev h10-eth0
qdisc htb 1: root refcnt 2 r2q 10 default 0 direct_packets_stat 17188 direct_qlen 1000
qdisc netem 500: parent 1:500 limit 1000 delay 50.0ms
qdisc netem 100: parent 1:100 limit 1000 delay 10.0ms
MaxiNet> h10 tc filter show dev h10-eth0
filter parent 1: protocol ip pref 1 u32
filter parent 1: protocol ip pref 1 u32 fh 800: ht divisor 1
filter parent 1: protocol ip pref 1 u32 fh 800::800 order 2048 key ht 800 bkt 0 flowid 1:500
  match 000001f4/ffffffff at 28
filter parent 1: protocol ip pref 1 u32 fh 800::801 order 2049 key ht 800 bkt 0 flowid 1:100
  match 00000064/ffffffff at 28
MaxiNet>
========================

root     10942  7494  0 14:36 pts/10   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.6 -f b -b 250000.0 -t 76.1236186018 -k 50
root     10955  7490  0 14:36 pts/8    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.23 -f b -b 1935688.55708 -t 10.8579653596 -k 100
root     10962  7502  0 14:36 pts/14   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.12 -f b -b 250000.0 -t 65.6257823718 -k 50
root     10970  7496  0 14:36 pts/11   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.18 -f b -b 1478168.95184 -t 76.5423183267 -k 100
root     10986  7500  0 14:36 pts/13   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.24 -f b -b 1248799.83444 -t 50.4681735561 -k 100
root     10991  7488  1 14:36 pts/7    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.11 -f b -b 5548016.7046 -t 46.372407258 -k 500
root     11004  7494  0 14:36 pts/10   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.6 -f b -b 978701.38447 -t 12.7329977769 -k 100
root     11017  7492  0 14:36 pts/9    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.30 -f b -b 1773768.2424 -t 86.9933250247 -k 100
root     11032  7479  1 14:36 pts/4    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.23 -f b -b 9444922.05225 -t 184.89502686 -k 500
root     11042  7498  0 14:36 pts/12   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.30 -f b -b 2481165.27172 -t 69.9687353423 -k 500
root     11052  7492  0 14:36 pts/9    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.30 -f b -b 1100476.38131 -t 39.1965822364 -k 100
root     11062  7498  0 14:36 pts/12   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.30 -f b -b 1745628.34068 -t 126.694791282 -k 100
root     11072  7479  0 14:36 pts/4    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.23 -f b -b 250000.0 -t 70.1106611252 -k 50
root     11077  7492  0 14:36 pts/9    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.10 -f b -b 250000.0 -t 24.0815360749 -k 50
root     11080  7488  0 14:36 pts/7    00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.6 -f b -b 250000.0 -t 131.593618664 -k 50
root     11083  7500  0 14:36 pts/13   00:00:00 /home/crowd/flexfcpf/iperf2-code/src/iperf -u -c 10.0.0.12 -f b -b 250000.0 -t 88.0423002832 -k 50
