from __future__ import division
import sys
import math
import random
from cp_flex_fcfs import *
import time

filename = 'Test_100_mesh.dat'

cpf = CPFlex(filename)

tstart = time.time()
cpf.cpgreedy()
tend = time.time()

print "State: " + cpf.state
if cpf.state == "NOT SOLVED":
	print "Remaining nodes: " + str([i for i in cpf.cn.V if not i in cpf.Controlled])
print "CRCs: " + str(len(cpf.CRCs)) 
print str(cpf.CRCs)
print "CLCs: " + str(len(cpf.CLCs)) + " (out of " + str(len(cpf.cn.C)) + " available)"
print str(cpf.CLCs)
print "Flows satisfied: " + str(len(cpf.Satisfied)) + " out of " + str(len(cpf.cn.F))
print "Runtime: " + str(tend - tstart) + " seconds"

#cpf.cn.output(legend=False)