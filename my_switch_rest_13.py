# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from webob import Response
from ryu.app.wsgi import ControllerBase, WSGIApplication, route
from ryu.lib import dpid as dpid_lib
from ryu.topology.api import get_switch, get_link, get_host


my_switch_instance_name = 'my_switch_api_app'
#url = '/myswitch/flowtable/{dpid}'

class MySwitchRest13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    _CONTEXTS = {'wsgi': WSGIApplication}

    def __init__(self, *args, **kwargs):
        super(MySwitchRest13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.flow_table = {}
        self.switches = {}
        wsgi = kwargs['wsgi']
        wsgi.register(SimpleSwitchController, {my_switch_instance_name: self})
#        wsgi.register(TopologyController, {'topology_api_app': self})

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        print("Function>>switch_features_handler>>Start")
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
#        match = parser.OFPMatch()
#        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
#                                          ofproto.OFPCML_NO_BUFFER)]
#        self.add_flow(datapath, 0, match, actions)
        datapath = ev.msg.datapath
        self.switches[datapath.id] = datapath
        self.mac_to_port.setdefault(datapath.id, {})

#    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        print("Function>>_packet_in_handler>>Start")
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, actions, msg.buffer_id)
                return
            else:
                self.add_flow(datapath, 1, match, actions)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        print("Function>>add_flow>>Start")
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst, idle_timeout=300)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst, idle_timeout=300)
        datapath.send_msg(mod)

    def del_flow(self, datapath, match):
        print("Function>>del_flow>>Start")
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod( datapath, command=ofproto.OFPFC_DELETE,
            out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY,
            priority=1, match=match)
        datapath.send_msg(mod)


    def set_mac_to_port(self, dpid, entry):
        print("Function>>set_mac_to_port>>Start")
        mac_table = self.mac_to_port.setdefault(dpid, {})
        datapath = self.switches.get(dpid)
        entry_port = entry['port']
        entry_mac = entry['mac']
        if datapath is not None:
            parser = datapath.ofproto_parser
            if entry_port not in mac_table.values():
#                for mac, port in mac_table.items():
#                    # from known device to new device
#                    actions = [parser.OFPActionOutput(entry_port)]
#                    match = parser.OFPMatch(in_port=port, eth_dst=entry_mac)
#                    self.add_flow(datapath, 1, match, actions)
#                    # from new device to known device
#                    actions = [parser.OFPActionOutput(port)]
#                    match = parser.OFPMatch(in_port=entry_port, eth_dst=mac)
#                    self.add_flow(datapath, 1, match, actions)
                mac_table.update({entry_mac: entry_port})
        return mac_table

    def set_flow_table(self, dpid, entry):
        print("Function>>set_flow_table>>Start")
        #flow_tab = self.flow_table.setdefault(dpid,{})
        datapath = self.switches.get(dpid)
        dl_type=entry['dltype']
        action = entry['action']
        if action == 'normal':
            action = ofproto_v1_3.OFPP_NORMAL

        ip_src = entry['ipsrc']
        ip_dest = entry['ipdest']
        if datapath is not None:
            parser = datapath.ofproto_parser
            # from known device to new device
            actions = [parser.OFPActionOutput(action)]
            if dl_type == "ip":
                port = entry['inport']
                match = parser.OFPMatch(in_port=port, ipv4_src=ip_src,
                    ipv4_dst=ip_dest, eth_type=0x0800)
            elif dl_type == "arp":
                match = parser.OFPMatch(arp_spa=ip_src,
                    arp_tpa=ip_dest, eth_type=0x0806)
            else:
                print("Wrong input!!")
            self.add_flow(datapath, 1, match, actions)
            #flow_tab.update({port:action})
        return "Added Successfully!!"

    def delete_flow_table(self, dpid, entry):
        print("Function>>delete_flow_table>>Start")
        #flow_tab = self.flow_table.setdefault(dpid,{})
        datapath = self.switches.get(dpid)
        dl_type=entry['dltype']
        ip_src = entry['ipsrc']
        ip_dest = entry['ipdest']
        if datapath is not None:
            parser = datapath.ofproto_parser
            # from known device to new device
            if dl_type == "ip":
                port = entry['inport']
                match = parser.OFPMatch(in_port=port, ipv4_src=ip_src,
                    ipv4_dst=ip_dest, eth_type=0x0800)
            elif dl_type == "arp":
                match = parser.OFPMatch(arp_spa=ip_src,
                    arp_tpa=ip_dest, eth_type=0x0806)
            else:
                print("Wrong input!!")
            self.del_flow(datapath, match)
            #del flow_tab[port]
        return "Deleted Succesfully!!"



class SimpleSwitchController(ControllerBase):
    def __init__(self, req, link, data, **config):
        super(SimpleSwitchController, self).__init__(req, link, data, **config)
        self.my_switch_app = data[my_switch_instance_name]

    @route('myswitch', '/myswitch/flowtable/{dpid}', methods=['GET'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def list_mac_table(self, req, **kwargs):
        print("Function>>list_mac_table>>Start")
        my_switch = self.my_switch_app
        dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        if dpid not in my_switch.mac_to_port:
            return Response(status=404)
        mac_table = my_switch.mac_to_port.get(dpid, {})
        body = json.dumps(mac_table)
        return Response(content_type='application/json', body=body)

    @route('myswitch', '/myswitch/flowtable/{dpid}', methods=['PUT'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def process_put_request(self, req, **kwargs):
        print("Function>>process_put_request>>Start")
        my_switch = self.my_switch_app
        dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        try:
            new_entry = req.json if req.body else {}
        except ValueError:
            raise Response(status=400)
        if dpid not in my_switch.mac_to_port:
            return Response(status=404)
        try:
            if new_entry.get('addmac'):
                mac_table = my_switch.set_mac_to_port(dpid, new_entry)
                body = json.dumps(mac_table)
            elif new_entry.get('addflow'):
                flow_table = my_switch.set_flow_table(dpid, new_entry)
                body = json.dumps(flow_table)
            elif new_entry.get('delflows'):
                flow_table = my_switch.delete_flow_table(dpid, new_entry)
                body = json.dumps(flow_table)
            else:
                print("Invalid Input!!")
            return Response(content_type='application/json', body=body)
        except Exception as e:
            return Response(status=500)

    @route('myswitch', '/myswitch/topology/switches', methods=['GET'])
    def list_switches(self, req, **kwargs):
        return self._switches(req, **kwargs)

    @route('myswitch', '/myswitch/topology/switches/{dpid}', methods=['GET'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def get_switch(self, req, **kwargs):
        return self._switches(req, **kwargs)

    @route('myswitch', '/myswitch/topology/links', methods=['GET'])
    def list_links(self, req, **kwargs):
        return self._links(req, **kwargs)

    @route('myswitch', '/myswitch/topology/links/{dpid}', methods=['GET'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def get_links(self, req, **kwargs):
        return self._links(req, **kwargs)

    @route('myswitch', '/myswitch/topology/hosts', methods=['GET'])
    def list_hosts(self, req, **kwargs):
        return self._hosts(req, **kwargs)

    @route('myswitch', '/myswitch/topology/hosts/{dpid}', methods=['GET'], requirements={'dpid': dpid_lib.DPID_PATTERN})
    def get_hosts(self, req, **kwargs):
        return self._hosts(req, **kwargs)

    def _switches(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        switches = get_switch(self.my_switch_app, dpid)
        body = json.dumps([switch.to_dict() for switch in switches])
        return Response(content_type='application/json', body=body)

    def _links(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        links = get_link(self.my_switch_app, dpid)
        body = json.dumps([link.to_dict() for link in links])
        # for link in links:
        #     print link.to_dict()
        return Response(content_type='application/json', body=body)

    def _hosts(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        hosts = get_host(self.my_switch_app, dpid)
        body = json.dumps([host.to_dict() for host in hosts])
        return Response(content_type='application/json', body=body)
