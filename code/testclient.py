


# listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# listener.sendto(data, ("localhost", 5001))
import binascii
import socket
import struct
import sys
import codecs
import datetime

# Create a TCP/IP socket
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = ('10.0.0.9', 10000)
client.connect(server_address)
data = codecs.decode("000001f4", "hex_codec")

try:

    # Send data
    print >>sys.stderr, 'sending "%s"' % data
    print "Before sending:"
    print datetime.datetime.now()
    sent = client.sendto(data, server_address)
    print "After sending:"
    print datetime.datetime.now()

    # Receive response
    print >>sys.stderr, 'waiting to receive'
    print "Before receiving:"
    print datetime.datetime.now()
    data, server = client.recvfrom(64)
    print "After receiving:"
    print datetime.datetime.now()
    print >>sys.stderr, 'received "%s"' % data

finally:
    print >>sys.stderr, 'closing socket'
    client.close()
