from __future__ import division
import sys
import math
#from pprint import pprint
import pylab as P
import numpy as np
from matplotlib.transforms import Bbox
from matplotlib.font_manager import FontProperties
from collections import defaultdict

labels = {}
labels["nodes"] = "Number of nodes" 
labels["#flows"] = "Number of flows" 
labels["#Satisfied"] = "Flows satisfied (%)" 
labels["#CRCs"] = "Number of CRCs used" 
labels["#CLCs"] = "Number of CLCs used" 
labels["#CLCDiff"] = "Number of new CLCs" 
labels["#nodeDiff"] = "New node assignments" 
labels["#flowDiff"] = "New flow assignments" 
labels["CRCpathlength"] = "Average CRC path length" 
labels["CLCpathlength"] = "Average CLC path length" 
labels["CLCload"] = "Average CLC load" 
labels["controlRatio"] = "CLC control ratio" 
labels["runtime"] = "runtime (s)" 

plot_path = "plots_sim/plots3"
results_path = "Simulation_Results"

results = {}
no_nodes = [36]
Properties = ["mesh","ring"]
obj = ["#Satisfied","#CRCs","#CLCs","#CLCDiff","#nodeDiff","#flowDiff","CRCpathlength","CLCpathlength","CLCload","controlRatio","runtime"]	

for n in no_nodes:
	results[n] = {}
	for p in Properties:
		results[n][p] = {}
		resultsfile = results_path + "/sim_results_pfo_Test_" + str(n) + "_" + p + ".dat"
		results[n][p]["counter"] = 0
		results[n][p]["flows"] = 0
		results[n][p]["sim"] = {}
		results[n][p]["scratch"] = {}
		for o in obj:
			results[n][p]["sim"][o] = 0
			results[n][p]["scratch"][o] = 0
		
		fin = open(resultsfile, "r")
		tmp = fin.readline()
		
		while True:
			tmp = fin.readline().split(" ")
			try:
				results[n][p]["counter"] += 1
				results[n][p]["flows"] += int(tmp[1])
				for i in range(0,6):
					results[n][p]["sim"][obj[i]] += int(tmp[i+2])
					results[n][p]["scratch"][obj[i]] += int(tmp[i+2+len(obj)])
				for i in range(6,len(obj)):
					results[n][p]["sim"][obj[i]] += float(tmp[i+2])
					results[n][p]["scratch"][obj[i]] += float(tmp[i+2+len(obj)])
			except:
				break

for n in no_nodes:
	for p in Properties:
		results[n][p]["flows"] /= results[n][p]["counter"]
		for o in obj:
			results[n][p]["sim"][o] /= results[n][p]["counter"]
			results[n][p]["scratch"][o] /= results[n][p]["counter"]

markers = ['^','*','o','v']
font = FontProperties(size=28)
font_smaller = FontProperties(size=18)
			
for o in obj:
	print "Creating graph for " + str(o)
	plotlabels = []
	plotlines = []
	
	y_label = labels[o]
	F = P.figure()
	AX1 = F.add_subplot(111)
	AX1.set_ylabel(y_label, fontproperties=font)
	ind = np.arange(len(no_nodes)*len(Properties))
	width = 0.35
	AX1.set_xticks(ind+width)
	AX1.set_xticklabels([str(p)+str(n) for n in no_nodes for p in Properties])
	
	ysim = []
	yscratch = []
	
	for n in no_nodes:
		for p in Properties:
			ysim.append(results[n][p]["sim"][o])
			yscratch.append(results[n][p]["scratch"][o])
			
	pl = AX1.bar(ind, ysim, width, color='blue')
	plotlines.append(pl[0])
	plotlabels.append("Reassignment")
	pls = AX1.bar(ind+width, yscratch, width, color='red')
	plotlines.append(pls[0])
	plotlabels.append("Scratch comparison")
	
	AX1.set_ylim(ymin=0)
	#AX1.set_yscale('log')
	for tick in AX1.xaxis.get_major_ticks():
		tick.label1.set_fontsize(24)
	for tick in AX1.yaxis.get_major_ticks():
		tick.label1.set_fontsize(24)
	P.savefig(plot_path + '/plot_' + str(o).replace("#","") + '.pdf', bbox_inches='tight')
	F = P.figure()
	F.legend(plotlines, plotlabels, loc='upper left', shadow=False, fancybox=True, prop=font_smaller)
	bb = Bbox.from_bounds(0, 0, 6.4, 4)
	P.savefig(plot_path + '/plot_legend_1_col.pdf', bbox_inches=bb)
	F = P.figure()
	F.legend(plotlines, plotlabels, loc='upper left', shadow=False, fancybox=True, prop=font_smaller, ncol=2)
	bb = Bbox.from_bounds(0, 0, 7, 2)
	P.savefig(plot_path + '/plot_legend_2_col.pdf', bbox_inches=bb)
			
		
