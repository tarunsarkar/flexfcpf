from __future__ import division
import sys, math, random, time, pdb
from math import exp,sqrt,pow
from cp_flex_fcfs import *

def loadlevel(i): # time i is provided in seconds
	i = (i/3600) % 24 # switch from seconds to day time
	if i <= 3:
		return 0.9*(-1/27*pow(i,2)+1)
	elif i <= 6:
		return 0.9*(1/27*pow(i-6,2)+1/3)
	elif i <= 15:
		return 0.9*(1/243*pow(i-6,2)+1/3)
	else:
		return 0.9*(-1/243*pow(i-24,2)+1)

def output(cpf,trun=None):
	print "State: " + cpf.state
	if cpf.state == "NOT SOLVED":
		print "Remaining nodes: " + str([i for i in cpf.cn.V if not i in cpf.Controlled])
	print "CRCs: " + str(len(cpf.CRCs)) 
	print str(cpf.CRCs)
	print "CLCs: " + str(len(cpf.CLCs)) + " (out of " + str(len(cpf.cn.C)) + " available)"
	print str(cpf.CLCs)
	print "Flows satisfied: " + str(len(cpf.Satisfied)) + " out of " + str(len(cpf.cn.F))
	if trun is not None:
		print "Runtime: " + str(trun) + " seconds"
		
def slimoutput(cpf,trun=None):
	if cpf.state == "NOT SOLVED":
		print "State: " + cpf.state + ", Remaining nodes: " + str(len([i for i in cpf.cn.V if not i in cpf.Controlled])) + " out of " + str(len(cpf.cn.V))
	else: 
		print "State: " + cpf.state
	print "CRCs: " + str(len(cpf.CRCs)) + ", CLCs: " + str(len(cpf.CLCs)) + " (out of " + str(len(cpf.cn.C)) + " available), Control ratio: " + str(cpf.CLCcontrolRatio()) \
			+ ", Average load: " + str(cpf.getAverageCLCload())
	print "Flows satisfied: " + str(len(cpf.Satisfied)) + " out of " + str(len(cpf.cn.F))
	if trun is not None:
		print "Runtime: " + str(trun) + " seconds"
		
def simstep(cpf,calctime):
	cpf2 = cpf.scratchCopy()
	tstart2 = time.time()
	cpf2.cpgreedy()
	tend2 = time.time()
	trun2 = tend2-tstart2
	
	nodeDiff2 = 0
	for v in cpf2.cn.V:
		nodeDiff2 += len(set(cpf2.cn.G.node[v]['CLCs']) - set(cpf.cn.G.node[v]['CLCs']))
	flowDiff2 = 0
	for f in cpf2.cn.F:
		if cpf2.cn.fdata[f]['CLC'] <> cpf.cn.fdata[f]['CLC']:
			flowDiff2 += 1

	tmpCLCs = list(cpf.CLCs)
	tstart = time.time()
	cpf.cpgreedy()
	tend = time.time()
	trun = tend-tstart
	nodeDiff = cpf.newCLCcontrols
	flowDiff = cpf.newFlowSats
		
	CLCDiff = len(set(cpf.CLCs) - set(tmpCLCs))
	CLCDiff2 = len(set(cpf2.CLCs) - set(tmpCLCs))

	if display_output:
		print "Time: " + str(calctime)
		slimoutput(cpf,trun)
		slimoutput(cpf2,trun2)
		print "\n"
		
	if graph_output:
		cpf.cn.output()
	
	global graph_output_once	
	if graph_output_once:
		graph_output_once = False
		cpf.cn.output(results_path + '/graph_fcfs_' + filename[:-4] + '.pdf')
		if only_graph_output:
			exit(1)
	
	if results_output and t >= 0:
		foutmain = open(results_filename, "a")
		foutmain.write(str(calctime) + " " + str(len(cpf.cn.F)) + " " + str(len(cpf.Satisfied)) + " " + str(len(cpf.CRCs)) + " " + str(len(cpf.CLCs)) + " " + str(CLCDiff) + " " + str(nodeDiff) + " " + str(flowDiff)  \
						+ " " + str(cpf.getAverageCRCpathlength()) + " " + str(cpf.getAverageCLCpathlength()) + " "  + str(cpf.getAverageCLCload()) + " "  + str(cpf.CLCcontrolRatio()) + " " + str(trun) \
						+ " " + str(len(cpf2.Satisfied)) + " " + str(len(cpf2.CRCs)) + " " + str(len(cpf2.CLCs)) + " " + str(CLCDiff2)  + " " + str(nodeDiff2) + " " + str(flowDiff2) \
						+ " " + str(cpf2.getAverageCRCpathlength()) + " " + str(cpf2.getAverageCLCpathlength()) + " "  + str(cpf2.getAverageCLCload()) + " " + str(cpf2.CLCcontrolRatio()) + " " + str(trun2) + " \n")
		foutmain.close() 
		
#sim_duration = 48*3600	
sim_duration = 3600	
stdll = 0.9
lltimeframe = 10.0
filename = 'Test_36_mesh.dat'
results_path = 'Simulation_Results'
results_filename = results_path + '/sim_results_pfo_fcfs_' + str(stdll) + '_' + str(lltimeframe) + '_' + filename
display_output = True
graph_output = False
results_output = False
graph_output_once = False
only_graph_output = False

cpf = CPFlex(filename)
cpf.flexOperation = True
cpf.clearFlows()
no_nodes = len(cpf.cn.V)

if results_output:
	foutmain = open(results_filename, "a")
	foutmain.write("time #flows #Satisfied #CRCs #CLCs #CLCDiff #nodeDiff #flowDiff CRCpathlength CLCpathlength CLCload controlRatio runtime " \
				+ "#SatisfiedScratch #CRCsScratch #CLCsScratch #CLCDiffScratch #nodeDiffScratch #flowDiffScratch CRCpathlengthScratch CLCpathlengthScratch CLCloadScratch controlRatioScratch runtimeScratch \n")
	foutmain.close()

t = -3600.0
tlast = t
perform_simstep = False
CLCloadlimit = stdll
lastdisp = -3600.0
calctime = -3600.0
fremhelp = {}
blacklist = []
llalarm = False
lambdamax = max([loadlevel(i) for i in range(0,25)])*no_nodes

while t < sim_duration:
	t += random.expovariate(lambdamax)
	c = random.random()
	if c < loadlevel(t)*no_nodes/lambdamax:
		d = random.expovariate(0.02)
		
		bltmp = list(blacklist)
		for f in bltmp:
			if cpf.cn.fdata[f]['isSat']:
				blacklist.remove(f)
			else:
				if math.ceil(cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration']) < math.ceil(cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration'] + t-tlast):
					fremhelp[math.ceil(cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration'])].remove(f)
					if not math.ceil(cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration'] + t-tlast) in fremhelp:
						fremhelp[math.ceil(cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration'] + t-tlast)] = []
					fremhelp[math.ceil(cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration'] + t-tlast)].append(f)
				cpf.cn.fdata[f]['duration'] += t-tlast
				
		for i in range(int(math.ceil(tlast)),int(math.ceil(t))):
			if i in fremhelp:
				tmp = list(fremhelp[i])
				for f in tmp:
					cpf.remFlow(f)
				del fremhelp[i]
		if math.ceil(t) in fremhelp:
			tmp = list(fremhelp[math.ceil(t)])
			for f in tmp:
				if cpf.cn.fdata[f]['stime'] + cpf.cn.fdata[f]['duration'] < t:
					cpf.remFlow(f)
					fremhelp[math.ceil(t)].remove(f)
		if t+d >= -10:
			cpf.addFlow(stime=t,dur=d)
			if not math.ceil(t+d) in fremhelp:
				fremhelp[math.ceil(t+d)] = []
			fremhelp[math.ceil(t+d)].append(cpf.cn.F[-1])
			
		tlast = t
		
		if cpf.state <> "Solved": # for reality purpose in case of a CLC or CRC failure, should currently not happen
			perform_simstep = True
			reason = 1

		if len(cpf.cn.F) > len(cpf.Satisfied) + len(blacklist) and len(cpf.cn.C) > len(cpf.CLCs):
			perform_simstep = True
			reason = 2
			
		if len(cpf.CLCs) > 1:
			if cpf.getAverageCLCload() < CLCloadlimit:
				if llalarm == False:
					tll = t
					llalarm = True
				if llalarm and t-tll > lltimeframe:
					llalarm = False
					tmplenCLCs = len(cpf.CLCs)
					loadtmp = cpf.getAverageCLCload()*tmplenCLCs
					while len(cpf.CLCs) > math.ceil(loadtmp):
						cpf.remCLC(cpf.getCLCwithLeastLoad())
					if len(cpf.CLCs) < tmplenCLCs:
						perform_simstep = True
						reason = 3
					else:
						CLCloadlimit -= 0.05
			else:
				llalarm = False
		
		if perform_simstep and t >= -10: 
			print "Reason: " + str(reason) + ", t = " + str(t)
			calctime = t
			lastdisp = t
			if t >= 0:
				simstep(cpf,calctime)
			else:
				cpf.cpgreedy()
				print "Using " + str(len(cpf.CLCs)) + " CLCs out of " + str(len(cpf.cn.C))
			if reason == 2 and len(cpf.cn.F) > len(cpf.Satisfied):
				btmp = list(set(cpf.cn.F) - set(cpf.Satisfied))
				for f in btmp:
					if not f in blacklist:
						blacklist.append(f)
			if reason == 3 and len(cpf.CLCs) >= tmplenCLCs:
				CLCloadlimit -= 0.05
			else:
				CLCloadlimit = stdll
			perform_simstep = False
			
		if t - lastdisp > 60:
			print "Current t: " + str(t)
			lastdisp = t	