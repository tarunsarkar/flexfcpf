import binascii
import socket
import struct
import sys
import datetime

# Create a TCP/IP socket
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = ('10.0.0.9', 10000)
server.bind(server_address)

while True:
    print >> sys.stderr, '\nwaiting to receive message'
    print "Before receiving:"
    print datetime.datetime.now()
    data, address = server.recvfrom(64)
    print "After receiving:"
    print datetime.datetime.now()

    print >> sys.stderr, 'received %s bytes from %s' % (len(data), address)
    print >> sys.stderr, data

    if data:
        print "Before sending:"
        print datetime.datetime.now()
        sent = server.sendto(data, address)
        print "After sending:"
        print datetime.datetime.now()
        print >> sys.stderr, 'sent %s bytes back to %s' % (sent, address)
