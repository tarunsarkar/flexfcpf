\documentclass[12pt,a4paper,oneside]{article}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[left=2.5cm,right=2.5cm,top=1.5cm,bottom=2cm]{geometry}
\usepackage{longtable} 
%multi-row
\usepackage{multirow}

% Graphics:
\usepackage{graphicx}
% All image are searched in this path:
\graphicspath{{figures/}}


\begin{document}
\title{{{\normalsize Master Thesis Proposal}}\\ 
 \vspace{\bigskipamount}
\textbf{{\large SDN testbed-based evaluation of }}\\
\textbf{{\large flow processing-aware controller placement}}\\
\textbf{{\normalsize Tarun Kumar Sarkar}} 
\\ {\normalsize To be advised by: Sébastien Auroux, M.Sc.}
\\ {\normalsize To be refereed by: Prof. Dr. Holger Karl}
\\ {\normalsize Computer Networks, University of Paderborn, Germany}}
\maketitle

\newpage
\tableofcontents{}
\newpage
\section{Background \& Motivation}
In recent years network traffic demand grew strongly and is growing continuously, both in terms of total volume, and the data rate of each user required. To cope with the consistently increasing traffic demands in mobile access networks, modern cellular network deployments become more and more dense and heterogeneous commonly known as DenseNets. But extending the underlying backhaul network is very costly, so an efficient management of such networks is required. Efficiently controlling the DenseNets is the hot topic of research today. Moreover trends like network function virtualization (NFV) lead to a growing relevance of data flow processing in the network. The data flow processing in the network imposes many other requirements on network resources and choosing the right locations for data processing is a challenging task and important for the network performance. Software-Defined Networking (SDN), which separates the control plane and the data plane, has been identified as a promising solution for an efficient control of DenseNets. However, controllers of classic SDN support the routing of information flow but do not support sophisticated processing mechanisms, e.g. NFV or Coordinated Multi-Point transmission (CoMP). Considering all of these aspects, recent work \cite{7127739} proposes flow processing-aware controllers, which are able to perform both efficient network control and data flow processing, extending the promising SDN approach. \par

Placing these controllers is again a challenging task, especially since the traffic load of a mobile network changes very quickly and the performance of a static placement will become worse over time. In modern dense and crowded networks, many data flows appear and expire every second. Also, to reduce reconfiguration overhead, any reassignment should take into account the existing placement. This problem is addressed by a flexible flow processing-aware controller placement framework (FlexFCPF) \cite{7343600}. FlexFCPF places flow processing-aware controllers and flexibly reassigns those based on the previous controller placement in reaction to network load changes. Figure-\ref{fig:fcpfnetwork} depicts how controller assignment happens in FlexFCPF.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=16cm,height=25cm,keepaspectratio]{fcpfnetwork}
    \caption{FlexFCPF network example}
    \label{fig:fcpfnetwork}
  \end{center}
\end{figure}


\section{Objective}
FlexFCPF has so far only been evaluated using simulations. The goal of the thesis is to evaluate it now in an emulated environment. To achieve that first thing I will have to do is to set up the testbed using Maxinet \cite{6857078}. Next I will have to understand how FlexFCPF works and will have to adapt it to run on the testbed. We already know the FlexFCPF address two main aspects, one is combining data flow processing in the network and the other is to assignment/reassignment of the controller in the network. I will have to investigate and use a suitable application for data flow processing and I will have to run FlexFCPF on the testbed with some realistic data to evaluate the second aspects. Additionally I will have to understand and adapt an alternative distributed algorithm for flow processing-aware controller placement.

\section{Approach}
To achieve the main goal of this thesis, we divided the whole work in eleven smaller specific and measurable milestones. In brief I am aiming to complete the following milestones as part of my thesis work:

\subparagraph{Knowledge building:}
Since the main goal is to setup a SDN testbed, the knowledge of OpenFlow, Open vSwitch and other SDN related topics \cite{onf} is necessary. The target of this task would be reading and some hands-on practice to work with Open vSwitch, SDN and OpenFlow. I followed a step by step approach to improve my learning. I start with setting up a test network with Mininet \cite{mininet}, and try different Mininet commands, create custom topology, interacting with host, switches to get familiar with Mininet. Then I tried adding flow rules in the Mininet switch and control the route of a flow based on an input path. During this exercise I got a better understanding of SDN and how OpenFlow works with Open vSwitch.

\subparagraph{Investigate and select a suitable controller software to run CLCs:}
One of the main aspects of this thesis is to assignment and reassignment of controller. For this reason I had to investigate which controller I can use in our testbed. The main criteria for selection was easy to implement, compatibility with the Maxinet, and customisable. There are many controller available in the OpenSource (POX \cite{pox}, Ryu \cite{ryu}, ONOS \cite{onos}, OpenDaylight \cite{odl} etc.). Based on discussion with supervisors and reading in the internet I tried some exercise with Ryu considering our thesis requirement. Ryu is developed in Python like Mininet/Maxinet, so it was easy for me as well to quickly start with it. I start with a Simple Switching hub implementation along with an custom topology in Mininet then move to more complicated implementation of Ryu (e.g. Traffic Monitoring, Link Aggregation, Spanning Tree, REST Linkage) to get familiar with Ryu. Finally I made a custom controller using the basic switching functionality and REST Linkage to test with a custom Mininet topology (4 switch 2 hosts). The purpose was to add desired flow entry in the switch through REST Linkage and achieve the flow routed through the path I wish. Later I tried to create a custom topology reading the input from files (keeping the CROWD architecture in mind) and also execute controller on the Mininet switch node. This is a kind of scenario which will be useful for my testbed setup for our FlexFCPF testing. Based on my hands on experience with Ryu, I found it is suitable for implementation in our testbed. It is component based, I can use the components I need and customise and extend according to the requirements during the course of the thesis. 

\subparagraph{Understanding of the problem and existing code:}
To work on this thesis I have to understand the problem statement and the optimisation model/constraints, then I must understand how the FlexFCPF and the distributed algorithm solves the problem. This algorithm is already been adapted to the simulation environment, understanding of the existing code would help me understanding the problem and the solution better. To adapt and run the FlexFCPF on the testbed I have to understand the complete flow of the FlexFCPF application. What are the component the application have, how they work together, what is the algorithm it uses to assign/reassign controller on the network, what is its input and what is its load balancing algorithm etc. The measurement of this task is dependent on the other two task i.e. Adaption of FlexFCPF and distributed algorithm on the testbed. Currently I am working on this task and simultaneously also trying to relate the knowledge I acquired with Mininet and Ryu exercise on how I can make all of them work together and design a Lab infrastructure. 

\subparagraph{Investigation on suitable traffic models to emulate the data flows:}
The goal of this task is to investigate, analyse different options to generate realistic network traffic for the testbed. So far I can think of two solutions, one is centralised (a process to generate flows for all nodes) or distributed (process running in each node to generate the flows). This includes find a way to generate data flows which represents data flows used in the existing simulation environment. The generator should be configurable and would have clear parametrised way of generating data flows. There is an existing tools Data Center TCP Traffic Generator (DCT2Gen) \cite{DBLP:journals/corr/WetteK14} is available, I will further investigate that tools. The completeness of this task can be measured based on the reasoning like the previous task.

\subparagraph{Investigate a suitable way to emulate the data flow processing:}
This also an investigation task like previous task, as part of this task I have to find a way to deal with packets or flows to emulate the data flow processing capability in the controller. The combined CPU power of the emulated nodes will be far bigger than the actual physical CPU power, for this reason  I will have to find a solution to scale the emulated processing capacity down to available physical resources. I also have to decide what kind of processing I would like to have in the controller, e.g. it could be a simple sleep timer or real processing (add a decoding algorithm and transform the received data before send it to destination). The completeness of the task can be measured based on a well-reasoned decision depending on theoretical (maybe also practical) analysis.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=16cm,height=25cm,keepaspectratio]{fcpfscenario}
    \caption{Typical Controller placement scenario}
    \label{fig:fcpfscenario}
  \end{center}
\end{figure}

\subparagraph{Testbed implementation using Maxinet:}
The goal of this task is to setup testbed using Maxinet, which emulates evaluation topologies that reflect the input of an FlexFCPF network based on the investigation results. FlexFCPF uses two-layer controller hierarchy as explained in the paper \cite{7127739} and the target architecture is called CROWD controller architecture. Figure-\ref{fig:fcpfscenario} illustrates a typical controller placement scenario and a possible control structure \cite{7127739} of CROWD. As part of this milestone I will have to understand MaxiNet \cite{6857078} and use it to emulate networks topologies and setup the network. Once I am familiarise with creating emulation  environment using Maxinet, then I will be able to create the testbed based on the CROWD architecture using Maxinet. At this stage I will also have to prepare a laboratory setup design, like where to place which components (of FlexFCPF and Maxinet), based on the available hardware resource. Finally the testbed should be able to initiate topology based on input file to mark the completion of this task.

\subparagraph{Adaptation of FlexFCPF:}
The goal of this task is to implement the FlexFCPF algorithm to run on the testbed. This would require the detailed understanding of all functions of FlexFCPF. This includes how all functions work together to  assign/reassign controller on the network, what input it takes, what is its load balancing algorithm etc. Finally the FlexFCPF has to be executable on the testbed. The completeness of the task can be measured based on implementation of several checks that confirm that things are working as they should be. E.g. flows are really routed correctly, knowledge of controller software in testbed matches placement decisions, etc.

\subparagraph{Adaptation of distributed algorithm:}
This is a similar task as the previous one, but with a different algorithm, depending on further input from the supervisor during the course of the thesis. Similarly to the previous task, the goal of this task is to implement the distributed algorithm to run on the testbed. This would also require the detailed understanding of all functions of distributed algorithm. The measurement criteria would be same like the measurement criterion for the adaptation of FlexFCPF.

\subparagraph{Testbed documentation:}
The purpose of this task is to document everything related to the testbed setup. This would be useful to prepare the same testbed in future starting from empty hardware. This document should have different section like hardware configuration, overview of the component allocation in the hardware, getting started and instruction to work with the testbed. The getting started section should include the installation instruction of all components and their configuration details along with the starting options of components. The target is to keep all the information available in simple and compact manner, so that it is easy to set up the testbed again without any hassle.

\subparagraph{Performing evaluation experiments:}
The goal of this task is to perform evaluation experiments with both (FlexFCPF and the distributed algorithm) algorithms on the testbed. Which means execute the experiments for a specific period of time with a specific network topology with the same work load on the testbed with both the algorithm separately and collect the result of theirs execution. The network emulation experiment parameter should be similar with the simulation environment e.g. 48 hours of execution with either 36 or 100 nodes and backhaul links with mesh or tree topology. This results will be used as base for the next task of comparison.

\subparagraph{Comparison of testbed results:}
The purpose of this task is to analyse the results collected form the previous evaluation task. Then compare the results against the simulation results. A detailed analysis of all differences between testbed results and simulation results should be done as part of this task. Every difference should be outlined and explained.

\section{Related Work}
The FlexFCPF is already been tested and evaluated in a simulation environment. There are many approach being tried for testing SDN based application like FlexFCPF. Mininet \cite{Lantz:2010:NLR:1868447.1868466} has been used to setup the emulation environment for most of the research like Container-Based Emulation \cite{Handigol:2012:RNE:2413176.2413206}, Maxinet \cite{6857078}. Some test systems have been built to extend container based emulation to run on a cluster. For example, CORE \cite{4753614} and Virtualization in the Emulab \cite{Hibler:2008:LVE:1404014.1404023} uses FreeBSD network stack virtualization. The approach explained in \cite{Handigol:2012:RNE:2413176.2413206} and \cite{Lantz:2010:NLR:1868447.1868466} is not suitable for large scale testbed setup and Emulab \cite{Hibler:2008:LVE:1404014.1404023} have issue with configuration and administration. I need a large emulation environment to evaluate FlexFCPF which is easy to configure and administer, as specified in Maxinet \cite{6857078} and CORE \cite{4753614} would fulfil our purpose. Since Maxinet support the existing Mininet API with minimal alteration and Mininet has been used in many SDN testbed emulations projects, we would like to proceed with Maxinet.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=16cm,height=25cm,keepaspectratio]{workplan}
    \caption{Preliminary Time Plan}
    \label{fig:workplan}
  \end{center}
\end{figure}

\section{Preliminary Time Plan}
The Gantt chart in Figure-\ref{fig:workplan} provides a rough estimate of the schedule for the realisation of this master thesis.

\bibliography{references}
\bibliographystyle{plain}

\end{document}

