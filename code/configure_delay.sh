#!/bin/sh

operation=$1
interface=$2
# root qdisc
r_handle=$3
# netem qdisc handle
n_handle=$4
delay=$5
flowid=$6
qdpresent=
delete=false
verbose=true

error() {
	printf "%s\n" "$1" >&2
}

log() {
	if [ $verbose = true ]; then
		printf "%s" "$1"

		if [ "$2" != false ]; then
			printf "\n"
		fi
	fi
}

usage() {
	echo "$0 [add|del] interface roothandle netemhandle delay flowid"
	exit 1
}

if [ "$interface" = '' ]; then
	error "No interface specified"
	usage
fi

if [ "$operation" = 'add' ]; then
	delete=false
elif [ "$operation" = 'del' ]; then
	delete=true
else
	error "Invalid operation '$operation', expected add or del"
	usage
fi


#
#	Check if we have our queue discipline already added to the target inerface
#	let's hope nothing else if using this handle
#
log "Checking if root qdisc already added to $interface... " false
if tc qdisc show dev "$interface" | grep "qdisc htb $r_handle:" > /dev/null; then
	log "yes"
	qdpresent=true
else
	log "no"
	qdpresent=false
fi

#
#	Nope, first add the new root queue discipline if required
#
if [ $qdpresent != true ]; then
	log "Adding qdisc with handle $r_handle... " false
	if tc qdisc add dev "$interface" root handle $r_handle: htb; then
		log "ok"
	else
		log "failed ($?)"
		exit 1
	fi
fi

#
#	This is the destination for the filters we will add below
#
if [ "$delete" != true ] && [ "$delay" != '' ]; then
	log "Checking if netem qdisc has been added... " false
	if tc qdisc show dev "$interface" | grep "qdisc netem $n_handle:" > /dev/null; then
		log "yes"
		log "Changing qdisc netem delay to ${delay}ms... " false
#		if tc qdisc change dev "$interface" parent $r_handle:$n_handle handle $n_handle: netem delay ${delay}ms; then
#			log "ok"
#		else
#			log "failed ($?)"
#			exit 1
#		fi
	else
		log "no"
		log "Adding class with classid $r_handle:$n_handle... " false
		if tc class add dev "$interface" parent $r_handle: classid $r_handle:$n_handle htb rate 2500Mbit; then
			log "ok"
		else
			log "failed ($?)"
			exit 1
		fi

		log "Adding qdisc netem with handle $n_handle (delay ${delay}ms)... " false
		if tc qdisc add dev "$interface" parent $r_handle:$n_handle handle $n_handle: netem delay ${delay}ms; then
			log "ok"
		else
			log "failed ($?)"
			exit 1
		fi
        log "Adding match condiftion for flowId $flowid to filter... " false

        # Add a filter to device $interface
        # - attach it to qdisc $r_handle:0
        # - apply it to IP packets
        # - with a prio/pref (priority) of 1 (this is arbitrary as all filters have the same priority)
        # - use the u32 classifier
        # - forward matching packets to clasid $n_handle:1
        # - match u32 $flowid 0xffffffff at 24
        if tc filter add dev "$interface" parent $r_handle:0 protocol ip prio 1 u32 classid 1:$n_handle match u32 $flowid 0xffffffff at 28; then
            log "ok"
        else
            log "failed ($?)"
            exit 1
        fi
	fi
fi

#
#	Add any flow id filters these classify the traffic and limit what's delayed
#
#if [ "$delete" != true ] && [ "$flowid" != '' ]; then
#    log "Adding match condiftion for flowId $flowid to filter... " false
#
#    # Add a filter to device $interface
#    # - attach it to qdisc $r_handle:0
#    # - apply it to IP packets
#    # - with a prio/pref (priority) of 1 (this is arbitrary as all filters have the same priority)
#    # - use the u32 classifier
#    # - forward matching packets to clasid $n_handle:1
#    # - match u32 $flowid 0xffffffff at 24
#    if tc filter add dev "$interface" parent $r_handle:0 protocol ip prio 1 u32 classid $n_handle:1 match u32 $flowid 0xffffffff at 28; then
#        log "ok"
#    else
#        log "failed ($?)"
#        exit 1
#    fi
#fi

